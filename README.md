<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

# Autenticação da API Laravel 8 usando o Passport

```
composer install
php artisan migrate
php artisan passport:install
php artisan passport:keys
php artisan storage:link
php artisan serve
```

## Limpar cache
```
php artisan cache:clear
php artisan config:clear
php artisan route:clear
php artisan optimize:clear
composer dumpautoload -o
```

## Teste local com docker

`docker-compose up -d`
`docker exec -u 0 -it lr_app sh`


{
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjQsImlhdCI6MTY1NjcyODI4NCwiZXhwIjoxNjU2ODE0Njg0fQ.Ftnxi1Ty1bzGaWs_lD-OX2D9hoWqGrytUAeiDwLRBaM",
    "UserId": 4,
    "Email": "teste@teste.com",
    "expires_in": "1 day"
}

# gamegos php-jws
## Supported Algorithms

Currently these algorithms are supported.

| alg Parameter    | Digital Signature or MAC Algorithm    |
|------------------|---------------------------------------|
| HS256            | HMAC using SHA-256                    |
| HS384            | HMAC using SHA-384                    |
| HS512            | HMAC using SHA-512                    |
| RS256<sup>1</sup>| RSASSA-PKCS-v1_5 using SHA-256        |
| RS384<sup>1</sup>| RSASSA-PKCS-v1_5 using SHA-384        |
| RS512<sup>1</sup>| RSASSA-PKCS-v1_5 using SHA-512        |
| none             | No digital signature or MAC performed |

- JWT: Json Web Token, o token propriamente dito;
- JWE: Json Web Encryption, a criptografia para assinatura do token;
- JWA: Json Web Algorithms, sobre os algoritmos para assinatura do token;
- JWK: Json Web Keys, as chaves para assinatura;
- JWS: Json Web Signature, sobre a assinatura do token.


See [JWA Cryptographic Algorithms for Digital Signatures and MACs](http://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-27#section-3) page for full list of defined algorithms for JWS.

- https://git.cybertron.fr/optimus/api-avocats/-/tree/a58e23ebe09cec2f69e42827d52da1b6d333d570

- https://pt.stackoverflow.com/questions/54687/como-ler-um-arquivo-de-certificado-digital-com-php
- https://groups.google.com/g/nfephp/c/0AWYCgvqQ8E
- https://www.brunobrito.net.br/jose-jwt-jws-jwe-jwa-jwk-jwks/
- https://blog.betrybe.com/tecnologia/jwt-json-web-tokens/
- https://www.sitepoint.com/php-authorization-jwt-json-web-tokens/

```
1. Ler o certificado enviado pela Losango contendo as chaves pública e privada; 
2. Gerar o JWK (JSON Web Key) informando a chave pública do certificado Losango; 
3. Gerar o header de assinatura e incluir o JWK gerado no passo acima; 
4. Gerar o payload da API de Solicitar de Token; 
5. Realizar a assinatura do payload utilizando o algorítmo RS256 e informando a chave privada do certificado Losango, o header e o payload gerados nos passos 3 e 4 respectivamente; 
6. Obter a chave pública Losango (API URL Chave Pública e API Chave Pública); 
7. Criptografar o payload assinado no passo 5 (utilizando o algorítmo RSA_OAEP e encriptação A256GCM) informando a chave pública Losango obtida no passo 6; 
8. Incluir o payload criptografado no header da requisição (DPoP);
```
