FROM php:7.4-fpm-alpine3.12
# FROM php:7.4-fpm-alpine

# Comment this to improve stability on "auto deploy" environments
RUN apk update && apk upgrade

# Install basic dependencies
RUN apk -u add bash git mc vim

# Install PHP extensions
ADD ./.docker/install-php.sh /usr/sbin/install-php.sh
RUN chmod +x /usr/sbin/install-php.sh
RUN /usr/sbin/install-php.sh

## wkhtmltopdf
RUN apk add --no-cache zlib zlib-dev libressl libressl-dev libffi

# on alpine static compiled patched qt headless wkhtmltopdf (47.2 MB)
RUN apk add --update --no-cache \
    wkhtmltopdf musl ca-certificates \
    qt5-qtbase \
    qt5-qtbase-x11 \
    qt5-qtsvg \
    qt5-qtwebkit \
    libgcc libstdc++ libx11 glib libxrender libxext libintl \
    ttf-dejavu ttf-droid ttf-freefont ttf-liberation \
    xvfb ttf-ubuntu-font-family \
    fontconfig
# Add openssl dependencies for wkhtmltopdf
RUN echo 'https://dl-cdn.alpinelinux.org/alpine/v3.8/main' >> /etc/apk/repositories
RUN apk add --update --no-cache libcrypto1.0 libssl1.0

# 2: Install the runtime dependency packages using apk:
RUN apk add --no-cache \
 libstdc++ \
 libx11 \
 libxrender \
 libxext \
 libssl1.0 \
 ca-certificates \
 fontconfig \
 freetype \
 ttf-dejavu \
 ttf-droid \
 ttf-freefont \
 ttf-liberation \
 ttf-ubuntu-font-family

RUN apk add --no-cache \
 g++ \
 git \
 gtk+ \
 gtk+-dev \
 make \
 mesa-dev \
 openssl-dev \
 patch \
 fontconfig-dev \
 freetype-dev

RUN ln -s /usr/bin/wkhtmltopdf /usr/local/bin/wkhtmltopdf;
RUN chmod +x /usr/local/bin/wkhtmltopdf;

COPY ./.docker/wkhtmltopdf /bin
RUN chmod -R 777 /bin/wkhtmltopdf

# RUN apk --no-cache --update --repository http://dl-cdn.alpinelinux.org/alpine/v$ALPINE_VERSION/main/ add postgresql-dev
# RUN apk add --upgrade libpq-dev
# RUN docker-php-ext-install pdo pdo_pgsql

# Copy existing application directory contents
COPY ./.docker/*.ini /usr/local/etc/php/conf.d/
COPY . .

# Change current user to www-data
USER www-data

# Expose ports and start php-fpm server
EXPOSE 9000
