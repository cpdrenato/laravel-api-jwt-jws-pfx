FROM nginx:1.17.1-alpine

ENV TZ America/Sao_Paulo

ARG NGINX_VERSION=1.17.1
ARG LIBRESSL_VERSION=3.1.3

ARG NGINX_DEVEL_KIT_VERSION=0.3.1

ARG NGINX_RTMP_MODULE_VERSION=1.2.1
ARG UPSTREAM_HC_VERSION=master
ARG QUICHE_VERSION=147bdf95a784eaaaaffe66d7a1fef505f516ddf3

ARG NGINX_DEVEL_KIT=ngx_devel_kit-${NGINX_DEVEL_KIT_VERSION}

ARG UPSTREAM_HC_MODULE=nginx_upstream_check_module-${UPSTREAM_HC_VERSION}
ARG NGINX_RTMP_MODULE=nginx-rtmp-module-${NGINX_RTMP_MODULE_VERSION}
ARG NGINX_ROOT=/etc/nginx
ARG WEB_DIR=/www
ARG GPG_KEYS=A1EB079B8D3EB92B4EBD3139663AF51BD5E4D8D5

ADD https://cloudflare.cdn.openbsd.org/pub/OpenBSD/LibreSSL/libressl-${LIBRESSL_VERSION}.tar.gz /tmp/libressl/libressl.tar.gz
ADD https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz /tmp/src/nginx.tar.gz

RUN apk update && apk upgrade && \
    apk add --no-cache \
    bash \
    openssh \
    openssh mc \
    mc \
    vim

RUN apk --no-cache add \
    libressl \
    build-base \
    linux-headers \
    pcre-dev \
    curl \
    zlib-dev \
    geoip-dev \
    libxslt-dev \
    perl-dev \
    gd-dev \
    unzip \
    zip \
    cmake \
    cargo \
    rust \
    git \
    go \
    ca-certificates \
    pcre \
    zlib \
    gd \
    geoip \
    libxslt \
    libgcc \
    certbot \
    certbot-nginx

RUN cd /tmp/src \
  && git clone --recursive https://github.com/cloudflare/quiche \
  && cd quiche \
  && git checkout ${QUICHE_VERSION}

RUN ln -sf /dev/stdout /var/log/nginx/access.log \
  && ln -sf /dev/stderr /var/log/nginx/error.log

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    echo $TZ > /etc/timezone && \
    apk add --no-cache openssl && \
    mkdir /etc/nginx/certificates && \
    openssl req \
      -x509 \
      -newkey rsa:2048 \
      -keyout /etc/nginx/certificates/key.pem \
      -out /etc/nginx/certificates/cert.pem \
      -days 365 \
      -nodes \
      -subj /CN=localhost && \
    mkdir /www && \
    mkdir -p /etc/ssl/certs && \
    if [ ! -f /etc/ssl/certs/dhparam.pem ]; then \
    openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048; \
    fi

  RUN apk del tzdata && \
    rm -rf /var/cache/apk/*

# Criar diretório raiz
RUN mkdir -p /var/www/html
RUN mkdir -p /var/www/app

# Emitir auto-certificação
RUN openssl genrsa 2048 > server.key \
#  && openssl req -new -key server.key -subj "/C=JP/ST=Tokyo/L=Chuo-ku/O=RMP Inc./OU=web/CN=localhost" > server.csr \
&& openssl req -new -key server.key -subj "/C=BR/ST=SAO PAULO/L=SAO PAULO/O=KLIOS/OU=web/CN=CA CERTIFICATE" > server.csr \
 && openssl x509 -in server.csr -days 3650 -req -signkey server.key > server.crt \
#  && openssl x509 -signkey CA.key -in CA.csr -req -days 3650 -out CA.pem \
#  && cp CA.pem /etc/nginx/CA.pem \
 && cp server.crt /etc/nginx/server.crt \
 && cp server.key /etc/nginx/server.key \
 && chmod 755 -R /var/www/html \
 && chmod 755 -R /var/www/app \
 && chmod 400 /etc/nginx/server.key

COPY common.conf /etc/nginx/common.conf
COPY ssl.conf /etc/nginx/conf.d/ssl.conf

# ***** CLEANUP *****
EXPOSE 80 443

VOLUME /etc/ssl
