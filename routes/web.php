<?php

use App\Http\Controllers\ContentController;
use App\Http\Controllers\PdfsController;
use App\Http\Controllers\LogsController;
use App\Http\Controllers\PfxCertController;
use App\Http\Controllers\OldController;
use App\Http\Controllers\JWTValidation;
use App\Http\Controllers\FullFlowController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});
Route::get('/', function () {
    return view('home');
});

Route::get('verificarphp', function () {
    return phpinfo();
});

Route::get('certificate', [PfxCertController::class, 'encode']);
Route::get('bundle', [JWTValidation::class, 'bundle']);
Route::get('full', [FullFlowController::class, 'show']);
Route::get('fulln', [FullFlowController::class, 'index']);
Route::get('/logs', [LogsController::class, 'show'])->name('logs');

// Rotas falhas
Route::get('teste', [OldController::class, 'certificate']);
Route::get('ger', function () {
    return view('certificate');
})->name('ger');
Route::post('gerador', [OldController::class, 'gerador']);

// Route::get('signature', [PfxCertController::class, 'signature']);
/*Route::get('relatorios/{id}', [PdfsController::class, 'show']);
Route::get('storage/{id}', [ContentController::class, 'show']);*/
