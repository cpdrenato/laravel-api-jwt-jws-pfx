<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PassportController;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\ConexaoController;
use App\Http\Controllers\Api\LoginApiTokenController;
use App\Http\Controllers\ItemsController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\OrdersController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->group(function () {
    Route::get('/autheduser', function () {
        return response()->json(auth()->user());
    });

    Route::apiResource('items', [ItemsController::class]);

    Route::apiResource('categories', [CategoriesController::class]);

    Route::apiResource('orders', [OrdersController::class])->only(['store']);

    Route::post('posttest', function (Request $request) {
        $data = $request->all();
        return response()->json($data);
    });
});*/

Route::get('/', function () {
    return "public endpoint";
});


Route::prefix('teste')->group(function () {
    //Route::post('register', [PassportController::class, 'register']);
    Route::post('login', [LoginApiTokenController::class, 'login']);
    // Verificar Conexão
    Route::get('conexao', [ConexaoController::class, 'conexao']);

    // put all api protected routes here
    // Route::middleware('auth:api')->group(function () {
    Route::middleware('jwtAuth')->group(function () {
        Route::post('pesquisa', [ApiController::class, 'buscarConsulta']);

    });
});
