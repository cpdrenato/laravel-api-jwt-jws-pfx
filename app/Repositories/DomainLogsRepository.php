<?php

namespace App\Repositories;

use Carbon\Carbon;
use App\Jobs\DomainLogsJob;

class DomainLogsRepository
{
    public function create($request, $auth, $model, $descricao) {
        $dados = [
            'usuario_id' => $auth->id ?? null,
            'usuario_ip' => $request->ip(),
            'acao' => $request->method(),
            'descricao' => $descricao,
            'log_datetime' => Carbon::now(),
            'url' => $request->fullUrl(),
            'model' => $model,
        ];

        dispatch(new DomainLogsJob($dados));
    }
}
