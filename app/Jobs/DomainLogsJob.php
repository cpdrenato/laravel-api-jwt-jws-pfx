<?php

namespace App\Jobs;

use App\Helpers\CustomLog;
use App\Models\DomainLog;
use Carbon\Carbon;

class DomainLogsJob extends Job
{
    protected $dados;

    public $tries = 5;

    public function __construct($dados)
    {
        $this->dados = $dados;
    }

    public function handle()
    {
        try {
            $time_exec_total = microtime(true);
            $domainLog = new DomainLog();

            $domainLog->usuario_id = $this->dados['usuario_id'];
            $domainLog->usuario_ip = $this->dados['usuario_ip'];
            $domainLog->acao = $this->dados['acao'];
            $domainLog->descricao = $this->dados['descricao'];
            $domainLog->log_datetime = $this->dados['log_datetime'];
            $domainLog->url = $this->dados['url'];

            $domainLog->modulo()->associate($this->dados['model']);
            $time_exec_total = microtime(true) - $time_exec_total;

            if ($domainLog->save()) {
                return $domainLog;
            } else {
                CustomLog::log('Domain_logs: NULL, exectime: ' . $time_exec_total, 'info', 'cron/cron-', 'cron');
                return null;
            }
        } catch (\Exception $exception) {
            CustomLog::log($exception->getMessage(), 'error', 'cron/cron-', 'cron');
            CustomLog::log($exception->getTraceAsString(), 'error', 'cron/cron-', 'cron');
        }
    }
}
