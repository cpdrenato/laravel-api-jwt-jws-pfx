<?php
/**
 * Created by vscode.
 * User: Renato Lucena
 * Date: 11/02/2019
 * Time: 08:00
 */
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class ConsultaShipped extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    public $conteudo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Array $conteudo)
    {
        $this->conteudo = $conteudo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->withSwiftMessage(
            function ($message) {
                if (property_exists($this, 'conteudo')) {
                    $message->model = (array) $this->conteudo;
                }
            }
        );

        $assunto = $this->conteudo['headerInfo'];

        return $this->subject($assunto)
            ->view("emails.consulta")
            ->with($this->conteudo);
    }
}
