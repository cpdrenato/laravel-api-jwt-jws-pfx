<?php

namespace App\Services;

use App\Repositories\DomainLogsRepository;

/**
 * Classe responsável pelos inserts dos DomainsLogs
 */
class DLRService
{
    public static function create($request, $user, $objeto, $descricao)
    {
        $domainLogsRepository = new DomainLogsRepository();
        $domainLogsRepository->create($request, $user, $objeto, $descricao);
    }
}
