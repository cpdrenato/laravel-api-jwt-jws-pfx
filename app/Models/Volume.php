<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Volume
 *
 * @property int $id
 * @property string $disco
 * @property int $volume
 * @property bool $ativo
 * @property string|null $synced_at
 * @property int|null $synced_id
 * @property int $arquivos
 * @property int $tamanho
 * @property int $limite_disco
 * @property int $limite_volume
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Volume whereArquivos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Volume whereAtivo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Volume whereDisco($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Volume whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Volume whereLimiteDisco($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Volume whereLimiteVolume($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Volume whereSyncedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Volume whereSyncedId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Volume whereTamanho($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Volume whereVolume($value)
 * @mixin \Eloquent
 */
class Volume extends Model
{
    protected $table = 'volumes';
    public $timestamps = false;
    protected $fillable = ['disco', 'volume'];
}
