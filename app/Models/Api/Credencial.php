<?php

namespace App\Models\Api;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Credencial extends Model
{
    protected $table = "adagio_api_credenciais";
    protected $primaryKey = "id";

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = [
        'email', 'password'
    ];

    protected $hidden = [
        'password'
    ];

    public function setEmailAttribute($valor)
    {
        if (isset($valor) && strlen($valor) > 0) {
            return $this->attributes['email'] = (string)mb_strtolower(trim($valor), 'UTF-8');
        }
    }
}
