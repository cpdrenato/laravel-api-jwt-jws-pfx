<?php

namespace App\Models\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PublicKey extends Model
{
    // use HasFactory;
    protected $primaryKey = 'kid';
    public $incrementing = false;
    public $fillable = ['kid', 'public_key'];
}
