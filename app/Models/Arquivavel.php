<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Arquivavel extends Model
{
    use SoftDeletes;

    protected $connection = "adagio";
    protected $table = 'arquivaveis';

    protected $fillable = [
        'arquivo_id',
        'arquivavel_id',
        'arquivavel_type',
        'descricao',
        'arquivo_type',
        'arquivo',
        'aprovado',
        'ordem',
        'ativo',
    ];
}
