<?php

namespace App\Models;

use SplFileInfo;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Filesystem\Filesystem;
use Aws\S3\S3Client;
use App\Models\Arquivo;
use App\Models\Volume;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use finfo;

class Disco
{
    public $volume;
    public $id;
    public $disco;
    //
    public $registro;
    //
    public $erro;
    public $atributos;
    public $hash;
    public $foiRecentementeCriado;

    public function __construct()
    {
        try {
            $this->ambiente = config("app.env", "local");
            $this->nome = $this->ambiente === 'local' ? 'local' : config("filesystems.default");
            $this->modelo = Volume::where('disco', $this->nome)->orderBy('volume', 'desc')->first();
            if (is_null($this->modelo)) {
                $this->modelo = Volume::create([
                    'ativo' => true,
                    'disco' => $this->nome,
                    'volume' => 1,
                    'limite_volume' => 2097152,
                    'limite_disco' => 0
                ]);
            }
            $this->id = $this->modelo->id;
        } catch (Exception $error) {
            //
        }
    }

    public function retributar()
    {
        $this->disco = $this->modelo->disco;
        $this->id = $this->modelo->id;
        $this->volume = $this->modelo->volume;
        $this->tipo = $this->arquivo->getMimeType();
        $this->unidade = head(preg_split("/\//", $this->tipo)) == 'application' || head(preg_split("/\//", $this->tipo)) == 'text' ? strtolower($this->arquivo->getClientOriginalExtension()) : head(preg_split("/\//", $this->tipo));
        $this->extensao = $this->unidade === 'image' ? 'jpg' : (preg_match('/^application\/pdf$/i', $this->tipo) === 1 ? 'pdf' : $this->arquivo->getClientOriginalExtension());
        $this->dispositivo = Storage::disk($this->disco);
        $this->tamanho = $this->arquivo->getSize() / 1024;
        $this->tamanhoTotal = $this->modelo->tamanho;
        $this->projecao = $this->tamanhoTotal + $this->tamanho;
        $this->hash = hash_file('sha256', $this->arquivo->getRealPath());

        if (empty($this->modelo->limite_volume) === false && $this->projecao > $this->modelo->limite_volume) {
            $this->volume += 1;
            $this->modelo = Volume::create([
                'ativo' => true,
                'disco' => $this->nome,
                'volume' => $this->volume,
                'limite_volume' => 2097152,
                'limite_disco' => $this->modelo->limite_disco
            ]);
        }

        $this->atributos['deleted_at'] = null;
        $this->atributos['volume_id'] = $this->modelo->id;
        $this->atributos['extensao'] = $this->extensao;
        $this->atributos['mimetype'] = $this->tipo;
        // $this-> atributos['sha256'] = $this-> hash;
        $this->atributos['tamanho'] = $this->tamanho;
        $this->atributos['unidade'] = $this->unidade;
    }

    public function salvar(&$arquivo)
    {
        try {
            if (empty($arquivo) === true) {
                throw new Exception("Arquivo is empty on saving.");
            }

            $this->arquivo = $arquivo;
            $this->retributar();
            $count = false;

            // DB::enableQueryLog();
            DB::beginTransaction();

            $this->registro = Arquivo::withTrashed()
                ->updateOrCreate(
                    array('sha256' => $this->hash),
                    $this->atributos
                );

            if ($this->registro->trashed()) {
                $this->registro->restore();
            }

            if (empty($this->registro->id) === false && empty($this->extensao) === false) {
                $this->destino = $this->unidade.'/'.$this->volume.'/'.$this->registro->id.'.'.$this->extensao;
            } else {
                throw new Exception("No ID found for model.");
            }

            $this->foiRecentementeCriado = $this->registro->wasRecentlyCreated;

            if ($this->foiRecentementeCriado === true) {
                $salvar = $this->dispositivo->put(
                    $this->destino,
                    file_get_contents($this->arquivo->getRealPath())
                );

                if ($salvar) {
                    $this->increment($this->tamanho);
                }
            }

            if ($this->foiRecentementeCriado === false) {
                $salvar = $this->dispositivo->exists($this->destino);
            }

            if ($salvar === false) {
                if ($this->foiRecentementeCriado) {
                    throw new Exception('Registro criado. Mas, arquivo ausente.');
                } else {
                    $candidato = hash_file("sha256", $this->arquivo->getRealPath());

                    if ($candidato === $this->hash) {
                        $anexado = $this->dispositivo->put(
                            $this->destino,
                            file_get_contents($this->arquivo->getRealPath())
                        );

                        if ($anexado === false) {
                            throw new Exception('Invalidez suspeita.');
                        } else {
                            $this->increment($this->tamanho);
                        }
                    } else {
                        throw new Exception('Registro ausente. E, arquivo ausente.');
                    }
                }
            }

            DB::commit();

            return true;
        } catch (Exception $erro) {
            DB::rollBack();
            $this->erro = $erro->getMessage();

            return false;
        }
    }

    private function increment($tamanho)
    {
        $this->modelo->increment('tamanho', $tamanho);
        $this->modelo->increment('arquivos', 1);
    }

    public function upload(
        &$request,
        &$arquivavel,
        $arquivavelId = 0,
        $querystring = "upload",
        $mimetype = "image",
        $atributos_extras = array(),
        $uniqueness = false,
        $atributos_pivot = array()
    ) {
        try {
            $resposta = array("error" => false, "errors" => [], "collection" => [], "model" => []);

            if ($request->hasFile($querystring) === false) {
                throw new Exception('No files.', 200);
            } else {
                $uploads = $request->file($querystring);
            }

            if (!is_array($uploads)) {
                $uploads = array($uploads);
            }

            $operacao = isSoftDelete($arquivavel)
                ? $arquivavel::withTrashed()->findOrFail($arquivavelId)
                : $arquivavel::findOrFail($arquivavelId);

            foreach ($uploads as $numero => $upload) {
                try {
                    $atributos = array();
                    $status = null;

                    $mimetypes = array(
                        "image" => ["extensions" => "jpeg,jpg,jpe,png,bmp,gif,jfif", "default" => "jpg"],
                        "image-original-size" => ["extensions" => "jpeg,jpg,jpe,png,bmp,gif,jfif", "default" => "jpg"],
                        "pdf" => ["extensions" => "pdf", "default" => "pdf"],
                        "any" => ["extensions" => "jpeg,jpg,jpe,png,gif,bmp,pdf,mp4,avi,mkv,wmv,mpeg,jfif", "default" => null],
                        "any-original-size" => ["extensions" => "jpeg,jpg,jpe,png,gif,bmp,pdf,jfif", "default" => null],
                        "video-and-image" => ["extensions" => "jpeg,jpg,bmp,png,gif,mp4,avi,mkv,wmv,mpeg,pdf,jfif", "default" => null],
                        "img-vid-doc" => ["extensions" => "jpeg,jpg,bmp,png,gif,mp4,avi,mkv,wmv,mpeg,pdf,xls,xlsx,csv,jfif", "default" => null],
                        "audio" => ["extensions" => "wav,mp3,ogg", "default" => null],
                    );

                    $validador = Validator::make (
                        [
                            'file'      => $upload,
                            'extension' => strtolower($upload->getClientOriginalExtension()),
                        ],
                        [
                            'file'           => 'required',
                            'extension'      => 'required|in:'.$mimetypes[$mimetype]["extensions"]
                        ]
                    );

                    if ($validador->fails()) {
                        throw new Exception(head($validador->errors()->all()), 415);
                    }

                    $atributos['arquivo'] = $upload->getClientOriginalName();
                    $atributos['tamanho_original'] = ($upload->getSize() /1024);
                    $atributos['mimetype_original'] = $upload->getClientMimeType();
                    $atributos['extensao_original'] = $upload->getClientOriginalExtension();
                    $atributos['originador'] = $request->user()->id;

                    $imagem = preg_match('/\bimage\//i', $upload->getClientMimeType()) === 1 ? true : false;
                    $tamanhoOriginal = preg_match('/original.size/i', $mimetype) === 1 ? true : false;

                    if ($imagem && getimagesize($upload->getRealPath()) && $tamanhoOriginal === true) {
                        list($width, $height, $type) = getimagesize($upload->getRealPath());

                        if ($type === IMAGETYPE_PNG) {
                            $image = imagecreatefrompng($upload->getRealPath());
                        } elseif ($type === IMAGETYPE_JPEG) {
                            $image = imagecreatefromjpeg($upload->getRealPath());
                        } elseif ($type === IMAGETYPE_GIF) {
                            $image = imagecreatefromgif($upload->getRealPath());
                        } elseif ($type === IMAGETYPE_BMP) {
                            $image = imagecreatefrombmp($upload->getRealPath());
                        } elseif ($type === IMAGETYPE_WBMP) {
                            $image = imagecreatefromwbmp($upload->getRealPath());
                        } else {
                            \Log::error('File format is invalid.');
                            throw new Exception("File format is invalid.");
                        }

                        imagejpeg($image, $upload->getRealPath());
                    } elseif ($imagem && getimagesize($upload->getRealPath()) && $tamanhoOriginal === false) {
                        list($width, $height, $type) = getimagesize($upload->getRealPath());
                        $percentWidth = $width > 800 ? (800 / $width) : 1;
                        $percentHeight = $height > 600 ? (600 / $height) : 1;
                        $percent = $percentWidth > $percentHeight ? $percentHeight : $percentWidth;
                        $new_width = $width * $percent;
                        $new_height = $height * $percent;
                        $new_image = imagecreatetruecolor($new_width, $new_height);

                        if ($type === IMAGETYPE_PNG) {
                            $image = imagecreatefrompng($upload->getRealPath());
                        } elseif ($type === IMAGETYPE_JPEG) {
                            $image = imagecreatefromjpeg($upload->getRealPath());
                        } elseif ($type === IMAGETYPE_GIF) {
                            $image = imagecreatefromgif($upload->getRealPath());
                        } elseif ($type === IMAGETYPE_BMP) {
                            $image = imagecreatefrombmp($upload->getRealPath());
                        } elseif ($type === IMAGETYPE_WBMP) {
                            $image = imagecreatefromwbmp($upload->getRealPath());
                        } else {
                            \Log::error('File format is invalid.');
                            throw new Exception("File format is invalid.");
                        }

                        imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                        imagejpeg($new_image, $upload->getRealPath(), 100);
                    }

                    if (is_array($atributos_extras) && count($atributos_extras) > 0) {
                        $atributos = array_merge($atributos_extras, $atributos);
                    }

                    $this->atributos = $atributos;

                    $consistencia = $this->salvar($upload);

                    if ($consistencia === false) {
                        throw new Exception($this->erro, 400);
                    }

                    $arquivoRepetidoNaRelacao = DB::table("arquivaveis")
                        ->where('arquivavel_type', isset($operacao->id_cartorial_dado) ? "App\CartorialDados" : "App\Models\\".class_basename($operacao))
                        ->where('arquivavel_id', isset($operacao->id_cartorial_dado) ? $operacao->id_cartorial_dado : $operacao->id)
                        ->where('arquivo_id', $this->registro->id);

                    if (isset($atributos["tipo"]) && intval($atributos["tipo"]) > 0) {
                        $arquivoRepetidoNaRelacao->where('arquivo_type', '=', $atributos["tipo"]);
                    }

                    $arquivoRepetidoNaRelacao = $arquivoRepetidoNaRelacao->get();

                    if ($arquivoRepetidoNaRelacao->count()) {
                        throw new Exception('Document file already related.', 200);
                    }

                    $status = 201;

                    $atributos_pivot['created_at'] = Carbon::now();

                    $documento = isset($atributos_pivot["arquivo_type"]) ? $atributos_pivot["arquivo_type"] : 20;
                    $operacao->arquivos()->attach($this->registro->id, $atributos_pivot);

                    $resposta['collection'][$numero] = array(
                        'error' => !$consistencia,
                        'errors' => [],
                        'model' => array(
                            'exigencia' => $uniqueness,
                            'exclusivo' => $this->foiRecentementeCriado,
                            'arquivo' => $this->hash,
                            'arquivo_nome' => $this->atributos["arquivo"],
                            'documento' => $documento,
                            'arquivavel' => $operacao->id,
                            'relacionamento' => get_class($operacao)
                        )
                    );
                } catch (Exception $loop) {
                    $status = $loop->getCode();
                    $mensagemStatus = $loop->getMessage();
                    $resposta['error'] = true;
                    $resposta['collection'][$numero] = array(
                        'error' => true,
                        'errors' => array($loop->getMessage()),
                        'model' => array()
                    );
                } finally {
                    if (is_null($status) === true) {
                        $status = 415;
                        $resposta['collection'][$numero] = array(
                            'error' => true,
                            'errors' => array('Unknown error'),
                            'model' => array()
                        );
                    }
                }
            }
        } catch (Exception $error) {
            \Log::error($error->getMessage());

            $status = $error->getCode() === 0 ? 500 : $error->getCode();
            $resposta['error'] = true;
            array_push($resposta['errors'], $error->getMessage());
        } finally {
            return response()->json($resposta, 200);
        }
    }

    public function uploadFromStorage(
        $nomesArquivos,
        &$arquivavel,
        $user,
        $arquivavelId = 0,
        $querystring = "upload",
        $mimetype = "image",
        $atributos_extras = array(),
        $uniqueness = false,
        $atributos_pivot = array()
    ) {
        try {
            set_time_limit(300);

            $resposta = array(
                "error" => false,
                "errors" => [],
                "collection" => [],
                "model" => []
            );

            $uploads = [];
            $finfo = new finfo(FILEINFO_MIME_TYPE);

            if (count($nomesArquivos) === 0) {
                $status = 200;
                return response('No files.', $status);
            }

            foreach ($nomesArquivos as $nomeArquivo) {
                if (Storage::disk('pcm')->exists($nomeArquivo.'.wav')) {
                    $fileReal = Storage::disk('pcm')->get($nomeArquivo.'.wav');

                    Storage::disk('local')->put('public/'.$nomeArquivo.'.wav', $fileReal);

                    $file_path = Storage::disk('local')->path('public/'.$nomeArquivo.'.wav');
                    $filename = $nomeArquivo.'.wav';

                    $upload = new UploadedFile(
                        $file_path,
                        $filename,
                        $finfo->file($file_path),
                        filesize($file_path),
                        0,
                        false
                    );

                    array_push($uploads, $upload);
                }
            }

            if (count($uploads) === 0) {
                $status = 200;
                return response('No files.', $status);
            }

            /*if ($request->hasFile($querystring) === false) {
                // Null
                throw new Exception('No files.', 200);
            } else {
                // Array<UploadedFile>
                $uploads = $request->file($querystring);
            }

            if (! is_array($uploads)) {
                // Array<UploadedFile>
                $uploads = array($uploads);
            }*/

            $ambiente = config("app.env", "local");
            $operacao = isSoftDelete($arquivavel)
                ?$arquivavel::withTrashed()->findOrFail($arquivavelId)
                :$arquivavel::findOrFail($arquivavelId);

            foreach ($uploads as $numero => $upload) {
                try {
                    $atributos = array();
                    $status = null;
                    $mimetypes = array(
                        "image" => ["extensions" => "jpeg,jpg,jpe,png,bmp,gif", "default" => "jpg"],
                        "image-original-size" => ["extensions" => "jpeg,jpg,jpe,png,bmp,gif", "default" => "jpg"],
                        "pdf" => ["extensions" => "pdf", "default" => "pdf"],
                        "any" => ["extensions" => "jpeg,jpg,jpe,png,gif,bmp,pdf,mp4,avi,mkv,wmv,mpeg", "default" => null],
                        "any-original-size" => ["extensions" => "jpeg,jpg,jpe,png,gif,bmp,pdf", "default" => null],
                        "video-and-image" => ["extensions" => "jpeg,jpg,bmp,png,gif,mp4,avi,mkv,wmv,mpeg,pdf", "default" => null],
                        "audio" => ["extensions" => "wav,mp3,ogg", "default" => null],
                    );

                    $validador = Validator::make(
                        array('upload' => $upload),
                        array('upload' => 'required|mimes:'.$mimetypes[$mimetype]["extensions"])
                    );

                    $atributos['arquivo'] = $upload->getClientOriginalName();
                    $atributos['tamanho_original'] = ($upload->getSize() /1024);
                    $atributos['mimetype_original'] = $upload->getClientMimeType();
                    $atributos['extensao_original'] = $upload->getClientOriginalExtension();
                    $atributos['originador'] = $user->id;

                    $imagem = preg_match('/\bimage\//i', $upload->getClientMimeType()) === 1 ? true : false;
                    $tamanhoOriginal = preg_match('/original.size/i', $mimetype) === 1 ? true : false;

                    if ($imagem && getimagesize($upload->getRealPath()) && $tamanhoOriginal === true) {
                        list($width, $height, $type) = getimagesize($upload->getRealPath());

                        if ($type === IMAGETYPE_PNG) {
                            $image = imagecreatefrompng($upload->getRealPath());
                        } elseif ($type === IMAGETYPE_JPEG) {
                            $image = imagecreatefromjpeg($upload->getRealPath());
                        } elseif ($type === IMAGETYPE_GIF) {
                            $image = imagecreatefromgif($upload->getRealPath());
                        } elseif ($type === IMAGETYPE_BMP) {
                            $image = imagecreatefrombmp($upload->getRealPath());
                        } elseif ($type === IMAGETYPE_WBMP) {
                            $image = imagecreatefromwbmp($upload->getRealPath());
                        } else {
                            \Log::error('File format is invalid.');
                            throw new Exception("File format is invalid.");
                        }

                        imagejpeg($image, $upload->getRealPath());
                    } elseif ($imagem && getimagesize($upload->getRealPath()) && $tamanhoOriginal === false) {
                        list($width, $height, $type) = getimagesize($upload->getRealPath());
                        $percentWidth = $width > 800 ? (800 / $width) : 1;
                        $percentHeight = $height > 600 ? (600 / $height) : 1;
                        $percent = $percentWidth > $percentHeight ? $percentHeight : $percentWidth;
                        $new_width = $width * $percent;
                        $new_height = $height * $percent;
                        $new_image = imagecreatetruecolor($new_width, $new_height);

                        if ($type === IMAGETYPE_PNG) {
                            $image = imagecreatefrompng($upload->getRealPath());
                        } elseif ($type === IMAGETYPE_JPEG) {
                            $image = imagecreatefromjpeg($upload->getRealPath());
                        } elseif ($type === IMAGETYPE_GIF) {
                            $image = imagecreatefromgif($upload->getRealPath());
                        } elseif ($type === IMAGETYPE_BMP) {
                            $image = imagecreatefrombmp($upload->getRealPath());
                        } elseif ($type === IMAGETYPE_WBMP) {
                            $image = imagecreatefromwbmp($upload->getRealPath());
                        } else {
                            \Log::error('File format is invalid.');
                            throw new Exception("File format is invalid.");
                        }

                        imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                        imagejpeg($new_image, $upload->getRealPath(), 100);
                    } else {
                        //
                    }

                    if (is_array($atributos_extras) && count($atributos_extras) > 0) {
                        $atributos = array_merge($atributos_extras, $atributos);
                    }

                    $this->atributos = $atributos;

                    $consistencia = $this->salvar($upload);

                    if ($consistencia === false) {
                        // 400 Bad Request
                        throw new Exception($this->erro, 400);
                    }

                    $arquivoRepetidoNaRelacao = DB::table("arquivaveis")
                        ->where('arquivavel_type', isset($operacao->id_cartorial_dado) ? "App\CartorialDados" : "App\Models\\".class_basename($operacao))
                        ->where('arquivavel_id', isset($operacao->id_cartorial_dado) ? $operacao->id_cartorial_dado : $operacao->id)
                        ->where('arquivo_id', $this->registro->id);

                    if (isset($atributos["tipo"]) && intval($atributos["tipo"]) > 0) {
                        $arquivoRepetidoNaRelacao->where('arquivo_type', '=', $atributos["tipo"]);
                    }

                    $arquivoRepetidoNaRelacao = $arquivoRepetidoNaRelacao->get();

                    if ($arquivoRepetidoNaRelacao->count()) {
                        // 200 OK
                        throw new Exception('Document file already related.', 200);
                    }

                    // 201 Created
                    $status = 201;
                    // Evidencia (20) is the default type
                    $documento = isset($atributos_pivot["arquivo_type"]) ? $atributos_pivot["arquivo_type"] : 20;
                    $operacao->arquivos()->attach($this->registro->id, $atributos_pivot);
                    $resposta['collection'][$numero] = array(
                        'error' => !$consistencia,
                        'errors' => [],
                        'model' => array(
                            'exigencia' => $uniqueness,
                            'exclusivo' => $this->foiRecentementeCriado,
                            'arquivo' => $this->hash,
                            'arquivo_nome' => $this->atributos["arquivo"],
                            'documento' => $documento,
                            'arquivavel' => $operacao->id,
                            'relacionamento' => get_class($operacao)
                        )
                    );
                } catch (Exception $loop) {
                    $status = $loop->getCode();
                    $mensagemStatus = $loop->getMessage();
                    $resposta['error'] = true;
                    $resposta['collection'][$numero] = array(
                        'error' => true,
                        'errors' => array($loop->getMessage()),
                        'model' => array()
                    );

                    \Log::error($loop->getCode());
                    \Log::error($loop->getMessage());
                    \Log::error($loop->getTraceAsString());
                    \Log::error(json_encode($resposta));
                } finally {
                    if (is_null($status) === true) {
                        $status = 415;
                        $resposta['collection'][$numero] = array(
                            'error' => true,
                            'errors' => array('Unknown error'),
                            'model' => array()
                        );
                    }

                    if (isset($upload) && ($upload instanceof SplFileInfo)) {
                        //@unlink($upload->getRealPath());
                    }

                    if (isset($image) && is_resource($image)) {
                        //@imagedestroy($image);
                    }

                    if (isset($new_image) && is_resource($new_image)) {
                        //@imagedestroy($new_image);
                    }
                }
            }
        } catch (Exception $error) {
            \Log::error($error->getMessage());
            \Log::error($error->getTraceAsString());

            $status = $error->getCode() === 0 ? 500 : $error->getCode();
            $resposta['error'] = true;
            array_push($resposta['errors'], $error->getMessage());
        } finally {
            return response()->json($resposta, $status);
        }
    }

    /**
     * Copia o arquivo
     *
     * @param Arquivo $arquivo     Arquivo
     * @param Arquivo $novoArquivo Novo Arquivo
     *
     * @return void
     */
    public function copiar($arquivo, $novoArquivo)
    {
        $ambiente = config("app.env", "local");
        $nome = $ambiente === 'local' ?
            'local' : config("filesystems.default");

        $caminhoArray = explode('/', $arquivo->getCaminho());
        $caminhoArray[1] = Volume::find($novoArquivo->volume_id)->volume;
        $caminhoArray[count($caminhoArray)-1] = $novoArquivo->id.'.'
            .$arquivo->extensao;
        $caminhoNovoArquivo = implode('/', $caminhoArray);
        Storage::disk($nome)->put($caminhoNovoArquivo, $arquivo->getBinary());
    }

    /**
     * Retorna o volume de acordo com o ambiente
     *
     * @return Collection
     */
    public function getVolume()
    {
        $ambiente = config("app.env", "local");
        $nome = $ambiente === 'local' ?
            'local' : config("filesystems.default");
        $volume = Volume::where('disco', $nome)->orderBy('volume', 'desc')
            ->first();
        return $volume;
    }

    /**
     * Copia o arquivo
     *
     * @param Arquivo $binario Binário
     * @param Arquivo $arquivo Novo Arquivo
     *
     * @return void
     */
    public function copiarBinario($binario, $arquivo)
    {
        $caminho =  $arquivo->unidade.DIRECTORY_SEPARATOR.$this->modelo->volume.
            DIRECTORY_SEPARATOR.$arquivo->id.'.'.$arquivo->extensao;
        Storage::disk($this->nome)->put($caminho, $binario);
    }
}
