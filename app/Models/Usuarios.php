<?php

namespace App\Models;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

class Usuarios extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    protected $connection = "adagio";
    protected $table = 'usuarios';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nome', 'documento', 'email', 'password', 'titular_id', 'transportadora', 'ramo_proprietario_id'];
    protected $dates = ['deleted_at'];
    protected $searchableColumns = [
        'email',
        'nome',
        'documento'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setNomeAttribute($valor)
    {
        if (isset($valor) && strlen($valor) > 0) {
            return $this->attributes['nome'] = (string)mb_strtoupper(trim($valor), 'UTF-8');
        }
    }

    public function setEmailAttribute($valor)
    {
        if (isset($valor) && strlen($valor) > 0) {
            return $this->attributes['email'] = (string)mb_strtolower(trim($valor), 'UTF-8');
        }
    }

}
