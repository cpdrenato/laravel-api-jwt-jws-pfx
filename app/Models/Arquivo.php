<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;

class Arquivo extends Model
{
    use SoftDeletes;
    protected $connection = "adagio";
    protected $table = 'arquivos';
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    protected $fillable = array(
        'id',
        'sha256',
        'unidade',
        'volume_id',
        'originador',
        'arquivo',
        'tamanho_original',
        'mimetype_original',
        'extensao_original',
        'tamanho',
        'mimetype',
        'extensao',
        'deleted_at',
        'classificacao',
        'titulo',
        'descricao',
        'tipo',
        'especie',
        'genero',
        'autor',
        'destinatario',
        'extras',
        'vencimento',
    );

    protected $appends = ['url', 'src'];

    public function getUrlAttribute() {
        return config('app.url') . '/storage/' . $this->sha256;
    }

    public function getSrcAttribute() {
        $src = $this->url;

        if ($this->extensao == 'pdf') {
            $src = '/images/pdf2.png';
        } elseif ($this->extensao == 'mp4' || $this->extensao == 'avi') {
            $src = '/images/video_thumbnail.jpg';
        }

        return $src;
    }

    public function originador()
    {
        return $this->belongsTo('App\Models\Usuarios', 'originador', 'id');
    }

    public function usuario()
    {
        return $this->belongsTo('App\Models\Usuarios', 'originador', 'id')->withTrashed();
    }

    /**
     * Recursos
     *
     * @return void
     */
    public function recursos()
    {
        return $this->morphedByMany(
            'App\Models\NcRecursos',
            'arquivavel',
            'arquivaveis'
        )->withPivot('descricao');
    }

    /**
     * Retornos
     *
     * @return void
     */
    public function retornos()
    {
        return $this->morphedByMany(
            'App\Models\NcRetorno',
            'arquivavel',
            'arquivaveis'
        )->withPivot('descricao');
    }

    /**
     * Deferimentos
     *
     * @return void
     */
    public function deferimentos()
    {
        return $this->morphedByMany(
            'App\Models\NcDeferimento',
            'arquivavel',
            'arquivaveis'
        )->withPivot('descricao');
    }

    /**
     * Indeferimentos
     *
     * @return void
     */
    public function indeferimentos()
    {
        return $this->morphedByMany(
            'App\Models\NcIndeferimento',
            'arquivavel',
            'arquivaveis'
        )->withPivot('descricao');
    }

    /**
     * Volume
     *
     * @return void
     */
    public function volume()
    {
        return $this->hasOne('App\Models\Volume', 'id', 'volume_id')->first();
    }

    /**
     * Retorna o arquivo binário em si
     *
     * @return void
     */
    public function getBinary()
    {
        return Storage::disk($this->volume()->disco)->get($this->getCaminho());
    }

    /**
     * Retorna o caminho do arquivo
     *
     * @return void
     */
    public function getCaminho()
    {
        return $this->unidade . '/' . $this->volume()->volume . '/' . $this->id . '.' . $this->extensao;
    }

    /**
     * Remove registro do arquivo no banco e storage.
     *
     * @return boolean
     */
    public function removerArquivo()
    {
        $statusDelete = $this->delete();
        $statusRemove = false;

        if ($statusDelete)
            $statusRemove = Storage::disk($this->volume()->disco)->delete($this->getCaminho());

        return $statusRemove;
    }

    public function getSizeStorage($folder)
    {
        //300 seconds = 2 minutes
        set_time_limit(120);
        $ambiente = config("app.env", "local");
        $nome = $ambiente === 'local' ? 'local' : config("filesystems.default");
        $disk = Storage::disk($nome);

        $size = array_sum(array_map(function ($file) {
            return (int) $file['size'];
        }, array_filter($disk->listContents($folder, true), function ($file) {
            return $file['type'] == 'file';
        })));

        $suffixes = array('bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');

        $power = $size > 0 ? floor(log($size, 1024)) : 0;

        $sizes = number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $suffixes[$power];
        \Log::debug(json_encode($sizes));
        return $sizes;
    }

    function byteconvert($bytes, $precision = 2)
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        return round($bytes, $precision) . ' ' . $units[$pow];
    }
}
