<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

Relation::morphMap([
    'arquivo' => 'App\Models\Arquivo'
]);


/**
 * App\Models\DomainLog
 *
 * @property int $id
 * @property int|null $usuario_id
 * @property string $usuario_ip
 * @property string $modulo_type
 * @property int|null $modulo_id
 * @property string $acao
 * @property string $descricao
 * @property \Carbon\Carbon $log_datetime
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $modulo
 * @property-read \App\Models\Usuarios|null $usuario
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DomainLog whereAcao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DomainLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DomainLog whereDescricao($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DomainLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DomainLog whereLogDatetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DomainLog whereModuloId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DomainLog whereModuloType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DomainLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DomainLog whereUsuarioId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DomainLog whereUsuarioIp($value)
 * @mixin \Eloquent
 */
class DomainLog extends Model
{

    protected $table = 'domain_logs';
    protected $primaryKey = "id";

    protected $fillable = [
        'usuario_id', 'usuario_ip', 'modulo', 'modulo_id', 'acao', 'descricao',
        'url'
    ];

    protected $dates = [
        'log_datetime', 'created_at', 'updated_at'
    ];

    public function usuario()
    {
        return $this->belongsTo(User::class, 'usuario_id', 'id');
    }

    public function modulo()
    {
        return $this->morphTo();
    }
}
