<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Auth;
use Exception;
use App\Jwt;
use App\Models\User;
use App\CognitoJWT;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        /*if (!$this->app->routesAreCached()) {
            Passport::routes();
        }*/
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.
        // $this->app['auth']->viaRequest('api', function ($request) {
        //     try {
        //         // Get the current session token
        //         $bearerToken = $request->bearerToken();
        //         // Not found in HTTP header
        //         if (empty($bearerToken)) {
        //             // Search somewhere else
        //             $bearerToken = $request->input("bearer_token", "");
        //             // Not found in HTTP querystring
        //             if (empty($bearerToken)) {
        //                 return null;
        //             }
        //         }

        //         $token_struct = (array) preg_split("/\./", $bearerToken);

        //         // Bad token
        //         if (count($token_struct) !== 3) {
        //             return null;
        //         }

        //         $payload = (array) JWT::jsonDecode(JWT::urlsafeB64Decode($token_struct[1]));
        //         $key = JWT::newPassword($request, $payload);
        //         $session = (array) JWT::decode($bearerToken, $key, true);

        //         // Maybe it is expirated
        //         if (empty($session)) {
        //             return null;
        //         }

        //         /*
        //         $user = Cache::remember("usuario-{$session["sub"]}", 5, function () use ($session) {
        //             return $user = User::with('papeis', 'titular', 'dependentes', 'grupos')
        //                 ->find($session["sub"]);
        //         });
        //         return $user;
        //         */
        //         return User::find($session["sub"]);
        //     } catch (Exception $error) {
        //         return null;
        //     }
        // });
        Auth::viaRequest('cognito', function ($request) {
            $jwt = $request->bearerToken();
            $region = env('AWS_REGION', '');//must be string
            $userPoolId = env('AWS_COGNITO_USER_POOL_ID', '');//must be string
            if ($jwt) {
                return CognitoJWT::verifyToken($jwt, $region, $userPoolId);
            }
            return null;
        });
    }
}
