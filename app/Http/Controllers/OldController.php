<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Gamegos\JWS\JWS;
use Firebase\JWT\JWT;
use App\AlphaPDF;
use App\FPDF;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Carbon\Carbon;

class OldController extends Controller
{
    public function signature()
    {
        $secretKey  = 'bGS6lzFqvvSQ8ALbOxatm7/Vk7mLQyzqaS34Q4oR1ew=';
        $token = JWT::decode($jwt, $secretKey, ['HS512']);
        $now = new \DateTimeImmutable();
        $serverName = "your.domain.name";

        if ($token->iss !== $serverName ||
            $token->nbf > $now->getTimestamp() ||
            $token->exp < $now->getTimestamp())
        {
            header('HTTP/1.1 401 Unauthorized');
            exit;
        }
    }

    public function certificate()
    {
        if ($_SERVER['HTTPS'] == 'on') { //Alterar a validação de https de acordo com seu servidor web
            echo "<table>";
            echo "<tr><th>Chave</th><th>valor</th></tr>";
            foreach ($_SERVER as $key => $value) {
                switch (true) {
                    //case strpos($key,"HTTP") == 0:
                    case preg_match("/^SSL_CLIENT(.*)$/", $key):
                        echo "<tr><td>", $key, "</td><td><pre>", $value, "</pre></td></tr>", PHP_EOL;
                        break;
                    default:
                        break;
                }
            }

            if ($_SERVER['SSL_CLIENT_CERT']) {
                $pub_key = openssl_pkey_get_public($_SERVER['SSL_CLIENT_CERT']);
                $keyData = openssl_pkey_get_details($pub_key);
                echo "<tr><td>Public Key Resource</td><td><pre>", $pub_key, "</pre></td></tr>", PHP_EOL;
                echo "<tr><td>Bits</td><td><pre>", $keyData["bits"], "</pre></td></tr>", PHP_EOL;
                echo "<tr><td>PUBLIC KEY</td><td><pre>", $keyData["key"], "</pre></td></tr>", PHP_EOL;
                echo "<tr><td>RSA N</td><td><pre>", $keyData["rsa"]['n'], "</pre></td></tr>", PHP_EOL;
                echo "<tr><td>RSA E</td><td><pre>", $keyData["rsa"]['e'], "</pre></td></tr>", PHP_EOL;
                echo "<tr><td>Type</td><td><pre>", $keyData['type'], "</pre></td></tr>", PHP_EOL;
                echo "<tr><td>Raw Key Data Key</td><td><pre>";
                var_dump($keyData);
                echo "</pre></td></tr>", PHP_EOL;

                openssl_pkey_free($pub_key);

                $cert = openssl_x509_read($_SERVER['SSL_CLIENT_CERT']);
                $certData = openssl_x509_parse($cert);
                foreach ($certData as $k => $d) {
                    echo "<tr><td>", strtoupper($k), "</td><td><pre>";
                    switch (gettype($d)) {
                        case "string":
                            echo $d;
                            break;
                        default:
                            var_dump($d);
                            break;
                    }

                    echo "</pre></td></tr>", PHP_EOL;
                }
                echo "<tr><td>Certificate Raw</td><td><pre>";
                var_dump($certData);
                echo "</pre></td></tr>", PHP_EOL;
                openssl_x509_free($cert);
            }


            echo "</table>";
        }
    }

    public function gerador()
    {

        setlocale( LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese' );
        date_default_timezone_set( 'America/Sao_Paulo' );
        // require('fpdf/alphapdf.php');
        // require('PHPMailer/class.phpmailer.php');


        // --------- Variáveis do Formulário ----- //
        $email    = $_POST['email'];
        $nome     = $_POST['nome'];
        $cpf      = $_POST['cpf'];

        // --------- Variáveis que podem vir de um banco de dados por exemplo ----- //
        $empresa  = "Universidade do Lincoln Borges";
        $curso    = "Workshop Segurança da Informação";
        $data     = "29/05/2017";
        $carga_h  = "8 horas";


        $texto1 = utf8_decode($empresa);
        $texto2 = utf8_decode("pela participação no ".$curso." \n realizado em ".$data." com carga horária total de ".$carga_h.".");
        $texto3 = utf8_decode("São Paulo, ".utf8_encode(strftime( '%d de %B de %Y', strtotime( date( 'Y-m-d' ) ) )));


        $pdf = new AlphaPDF();

        // Orientação Landing Page ///
        $pdf->AddPage('L');

        $pdf->SetLineWidth(1.5);


        // desenha a imagem do certificado
        $pdf->Image('certificado.jpg',0,0,295);

        // opacidade total
        $pdf->SetAlpha(1);

        // Mostrar texto no topo
        $pdf->SetFont('Arial', '', 15); // Tipo de fonte e tamanho
        $pdf->SetXY(109,46); //Parte chata onde tem que ficar ajustando a posição X e Y
        $pdf->MultiCell(265, 10, $texto1, '', 'L', 0); // Tamanho width e height e posição

        // Mostrar o nome
        $pdf->SetFont('Arial', '', 30); // Tipo de fonte e tamanho
        $pdf->SetXY(20,86); //Parte chata onde tem que ficar ajustando a posição X e Y
        $pdf->MultiCell(265, 10, $nome, '', 'C', 0); // Tamanho width e height e posição

        // Mostrar o corpo
        $pdf->SetFont('Arial', '', 15); // Tipo de fonte e tamanho
        $pdf->SetXY(20,110); //Parte chata onde tem que ficar ajustando a posição X e Y
        $pdf->MultiCell(265, 10, $texto2, '', 'C', 0); // Tamanho width e height e posição

        // Mostrar a data no final
        $pdf->SetFont('Arial', '', 15); // Tipo de fonte e tamanho
        $pdf->SetXY(32,172); //Parte chata onde tem que ficar ajustando a posição X e Y
        $pdf->MultiCell(165, 10, $texto3, '', 'L', 0); // Tamanho width e height e posição

        $pdfdoc = $pdf->Output('', 'S');



        // ******** Agora vai enviar o e-mail pro usuário contendo o anexo
        // ******** e também mostrar na tela para caso o e-mail não chegar

        $subject = 'Seu Certificado do Workshop';
        $messageBody = "Olá $nome<br><br>É com grande prazer que entregamos o seu certificado.<br>Ele está em anexo nesse e-mail.<br><br>Atenciosamente,<br>Lincoln Borges<br><a href='http://www.lnborges.com.br'>http://www.lnborges.com.br</a>";


        $mail = new PHPMailer();
        $mail->SetFrom("certificado@lnborges.com.br", "Certificado");
        $mail->Subject    = $subject;
        $mail->MsgHTML(utf8_decode($messageBody));
        $mail->AddAddress($email);
        $mail->addStringAttachment($pdfdoc, 'certificado.pdf');
        $mail->Send();


        $pdf->Output();


    }
}
