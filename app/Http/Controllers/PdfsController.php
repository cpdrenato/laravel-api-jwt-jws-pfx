<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Crypt;

class PdfsController extends Controller
{
    public function show(Request $request, $id) {
        try {
            $protocolo = Crypt::decrypt($id);
            $codigos = preg_split('/\./', $protocolo);
            $metodo = head($codigos);

            return $this->$metodo($request, $codigos);
        } catch (Exception $error) {
            return "Relatório não encontrado.";
        }
    }
}
