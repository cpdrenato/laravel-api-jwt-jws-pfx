<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Gamegos\JWS\JWS;
use Firebase\JWT\JWT;
use \Firebase\JWT\JWK;
use App\AlphaPDF;
use App\FPDF;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;


class PfxCertController extends Controller
{
    public function certificate()
    {
        //Caminho do Certificado
        $pfxCertPrivado = Storage::disk('local')->get('certificado/certificado.pfx'); // //'certificado.pfx';
        // $pfxCertPrivado = Storage::get('certificado/certificado.pfx');
        $cert_password  = 'idsrv3test';

        // if (!file_exists($pfxCertPrivado)) {
        //     echo "Certificado não encontrado!! " . $pfxCertPrivado;
        // }

        // $pfxContent = file_get_contents(storage_path('certificado') . 'certificado.pfx');
        $pfxContent = $pfxCertPrivado;


        if (!openssl_pkcs12_read($pfxContent, $x509certdata, $cert_password)) {
            echo "O certificado não pode ser lido!!";
        } else {

            $CertPriv   = array();
            $CertPriv   = openssl_x509_parse(openssl_x509_read($x509certdata['cert']));

            $PrivateKey = $x509certdata['pkey'];

            $pub_key = openssl_pkey_get_public($x509certdata['cert']);
            $keyData = openssl_pkey_get_details($pub_key);

            $PublicKey  = $keyData['key'];

            echo '<br>'.'<br>'.'--- Dados do Certificado ---'.'<br>'.'<br>';
            echo $CertPriv['name'].'<br>';                           //Nome
            echo $CertPriv['hash'].'<br>';                           //hash
            // echo $CertPriv['subject']['C'].'<br>';                   //País
            // echo $CertPriv['subject']['ST'].'<br>';                  //Estado
            // echo $CertPriv['subject']['L'].'<br>';                   //Município
            echo $CertPriv['subject']['CN'].'<br>';                  //Razão Social e CNPJ / CPF
            echo date('d/m/Y', $CertPriv['validTo_time_t'] ).'<br>'; //Validade
            // echo $CertPriv['extensions']['subjectAltName'].'<br>';   //Emails Cadastrados separado por ,
            // echo $CertPriv['extensions']['authorityKeyIdentifier'].'<br>';
            // echo $CertPriv['issuer']['OU'].'<br>';                   //Emissor
            echo '<br>'.'<br>'.'--- Chave Pública ---'.'<br>'.'<br>';
            print_r($PublicKey);
            echo '<br>'.'<br>'.'--- Chave Privada ---'.'<br>'.'<br>';
            echo $PrivateKey;

            return $CertPriv;
        }
    }
    public function encode()
    {
        // Encoding
        $headers = array(
            'alg' => 'HS256', //alg is required. see *Algorithms* section for supported algorithms
            'typ' => 'JWT'
        );
        $issuedAt   = new \DateTimeImmutable();
        $expire     = $issuedAt->modify('+6 minutes')->getTimestamp();      // Add 60 seconds
        $serverName = env('APP_URL');//"your.domain.name";
        $username   = "Renato";
        $sub = [
            'certificado' => self::certificate(), //'12911536',
            'email' => 'renato@example.com',
        ];
        // anything that json serializable
        $payload = array(
            'certificado' => self::certificate(),
            'sub' => $sub, //'renato@example.com',
            'iss'  => $serverName,
            'nbf'  => $issuedAt->getTimestamp(),
            'iat' => $issuedAt->getTimestamp(), //'1402993531',
            'exp'  => $expire,                           // Expire
            'userName' => $username,
        );
        // para gerar o token novo para o .env //bin2hex(openssl_random_pseudo_bytes(64)); //bin2hex(random_bytes(64)); //
        $key =  env('JWT_SECRET'); //'some-secret-for-hmac';

        echo "SECRET=" . $key . "\n";

        $jws = new \Gamegos\JWS\JWS();

        $result = $jws->encode($headers, self::convert_from_latin1_to_utf8_recursively($payload), $key);

        dump($result);

        self::decode($jws->encode($headers, self::convert_to_utf8_recursively($payload), $key), $key);
    }

    public function decode($encode, $key)
    {
        try {
            // $key = 'some-secret-for-hmac';
            //jws encoded string
            $jwsString = $encode;

            $jws = new \Gamegos\JWS\JWS();

            $result = $jws->verify($jwsString, $key);
            dump($result);

        } catch (\Exception $error) {
            \Log::error($error->getMessage());
            dump($error->getMessage());
            return new Response($error->getMessage() . ' Acesso nao autorizado', 401);
        }


    }

    /**
     * Encode array from latin1 to utf8 recursively
     * @param $dat
     * @return array|string
    */
    public static function convert_from_latin1_to_utf8_recursively($dat)
    {
       if (is_string($dat)) {
          return utf8_encode($dat);
       } elseif (is_array($dat)) {
          $ret = [];
          foreach ($dat as $i => $d) $ret[ $i ] = self::convert_from_latin1_to_utf8_recursively($d);

          return $ret;
       } elseif (is_object($dat)) {
          foreach ($dat as $i => $d) $dat->$i = self::convert_from_latin1_to_utf8_recursively($d);

          return $dat;
       } else {
          return $dat;
       }
    }

    function convert_to_utf8_recursively($dat){
        if( is_string($dat) ){
            return mb_convert_encoding($dat, 'UTF-8', 'UTF-8');
        }
        elseif( is_array($dat) ){
            $ret = [];
            foreach($dat as $i => $d){
                $ret[$i] = self::convert_to_utf8_recursively($d);
            }
            return $ret;
        }
        else{
            return $dat;
        }
    }
}
