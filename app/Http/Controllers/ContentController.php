<?php

namespace App\Http\Controllers;

use DB;

use Illuminate\Support\Facades\Storage;

class ContentController extends Controller
{
    public function show($sha256) {

        $arquivo = $adagio->table("arquivos")->where('sha256', '=', $sha256)->first();

        if (is_null($arquivo)) {
            return response(null, 204);
        }

        $volume = $adagio->table('volumes')->where('id', $arquivo->volume_id)->first();

        $caminho = $arquivo->unidade.'/'.$volume->volume.'/'.$arquivo->id.
            '.'.$arquivo->extensao;
        $disco = $volume->disco;

        if ($volume->id == 6) {
            $disco = 's3';
            $caminho = 'imagens-giap/'.$arquivo->id.'.'.$arquivo->extensao;
        }

        $extensionsToDirectDownload = ['csv','xls','xlsx'];
        $nomeArquivo = $this->retiraAcentos($arquivo->arquivo);

        if (in_array(strtolower($arquivo->extensao), $extensionsToDirectDownload)) {
            return Storage::download($caminho, $nomeArquivo);
        }
        $arquivoConteudo = Storage::disk($disco)->get($caminho);

        return response($arquivoConteudo, 200)
            ->header('Content-Type', $arquivo->mimetype)
            ->header('Content-disposition', "inline; filename={$nomeArquivo}");
    }

    public function retiraAcentos($str) {
        /*$string = 'ÁÍÓÚÉÄÏÖÜËÀÌÒÙÈÃÕÂÎÔÛÊáíóúéäïöüëàìòùèãõâîôûêÇç?';*/
        $charset = 'utf-8';
        $str = htmlentities($str, ENT_NOQUOTES, $charset);
        $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str);
        $str = preg_replace('#&[^;]+;#', '', $str);
        return $str;
    }
}
