<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Arquivavel;
use App\Http\Controllers\Api\LoginApiTokenController;
use App\Services\DLRService;
use App\Http\Controllers\Controller;
use App\Repositories\DomainLogsRepository;
use Illuminate\Http\Request;
use Validator;
use Exception;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;

/**
 * @category Controller
 * @package  App\Http\Controllers\Api\
 * @author   Renato Lucena <cpdrenato@gmail.com>
 * @copyright (c) 2021-2022 Renato Lucena
 * @license Copyright 2021 Klios. Released under dual MIT and GPL licenses.
 * @link     https://adagio.klios.com.br/api
 */

class ApiController extends Controller
{


    public function __construct() {

    }

    public function buscarConsulta(Request $request)
    {
        try {
            $checar = LoginApiTokenController::checarToken($request);

            if ($checar->original == 500) {
                return response()->json('Acesso nao autorizado ou token invalido!', 401);
            }

            // return response()->json(['consulta' => $consulta], 200);
        } catch (Exception $error) {
            \Log::error($error->getMessage());
            \Log::error($error->getTraceAsString());
            $adagio = new Adagio($error->getMessage(), 500);
            // Bad Request response
            return response()->json([
                'error' => $adagio->response([])
            ], 500);
        }
    }









}
