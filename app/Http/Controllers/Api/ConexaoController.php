<?php

namespace App\Http\Controllers\Api;

use App\Repositories\DomainLogsRepository;
use App\Services\DLRService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;

/**
 * @category Controller
 * @package  App\Http\Controllers\Api\
 * @author   Renato Lucena <cpdrenato@gmail.com>
 * @copyright (c) 2022 Renato Lucena
 * @license Copyright 2022 Klios. Released under dual MIT and GPL licenses.
 * @link     https://renatolucena.net
 */

class ConexaoController extends Controller
{
    public function conexao()
    {
        try {
            $dbSemantic = DB::connection('jwtjws');
            // DB::connection()->getPdo();
            $dbSemantic->getPdo();

            if ($dbSemantic->getDatabaseName()) {
                //echo "Yes! Successfully connected to the DB: " . $dbSemantic->getDatabaseName();
                return response()->json("Yes! Successfully connected to the DB: " . $dbSemantic->getDatabaseName(), 200);
            } else {
                die("Could not find the database. Please check your configuration.");
            }
        } catch (\Exception $e) {
            echo $e;
            die("Could not open connection to database server.  Please check your configuration.");
        }
    }
}
