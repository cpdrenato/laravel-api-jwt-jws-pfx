<?php

namespace App\Http\Controllers\Api;

use App\Helpers\CustomLog;
use App\Models\Api\Credencial;
// use App\Zen\Api\Credencial;
use App\Services\DLRService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Exception;
use Illuminate\Support\Facades\DB;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

/**
 * @category Controller
 * @package  App\Http\Controllers\Api\
 * @author   Renato Lucena <cpdrenato@gmail.com>
 * @copyright (c) 2021-2022 Renato Lucena
 * @license Copyright 2021 Klios. Released under dual MIT and GPL licenses.
 * @link     https://renatolucena.net
 */

class LoginApiTokenController extends Controller
{
    private $request;
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    /**
     * Create a new token.
     *
     * @param  \App\Zen\Credencial   $user
     * @return string
     */
    protected function jwt(Credencial $Credencial)
    {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $Credencial->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60 * 60 * 24 //time() + 60 * 60 // time() + 60 * 60 | Expiration time - 60 mins (60 * 60 = 3600 seconds (1 hour))
        ];
        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    /**
     * Authenticate a Credencial and return the token if the provided credentials are correct.
     *
     * @param  \App\Zen\Credencial   $Credencial
     * @return mixed
     */
    public function login(Request $request)
    {
        CustomLog::log('LOGIN API V2', 'INFO', 'api/api-', 'api');

        $this->validate($request, [
            'email' => ['required', 'string', 'email', 'max:255'],
            'password'  => ['required', 'string']
        ]);
        // Find the Credencial by email
        $Credencial = Credencial::where('email', $request->input('email'))->first();
        CustomLog::log('LOGIN ID: ' . $Credencial->id . ' LOGIN EMAIL: ' . $request->input('email'), 'INFO', 'api/api-', 'api');
        DLRService::create($request, $Credencial->id, $Credencial, 'API Login');
        if (!$Credencial) {
            return response()->json([
                'error' => 'Email does not exist.'
            ], 400);
        }
        $credencialFirst = DB::table('users')->where('id', $Credencial->id)->first();
        $expiresAt = Carbon::now()->addHours(24);
        //dd($expiresAt->getTimestamp());
        // if (isset($credencialFirst->id)) {
        //     DB::table('user')
        //         ->where('id', $credencialFirst->id)
        //         ->update([
        //             'token' => $this->jwt($Credencial),
        //             'id' => $Credencial->id,
        //             'updated_at' => Carbon::now(),
        //             'expires_at' => $expiresAt
        //         ]);
        // } else {
        //     DB::table('credencial_token')
        //         ->updateOrInsert([
        //             'token' => $this->jwt($Credencial),
        //             'credencial_id' => $Credencial->id,
        //             'created_at' => Carbon::now(),
        //             'updated_at' => Carbon::now(),
        //             'expires_at' => $expiresAt
        //         ]);
        // }

        // Verify the password and generate the token
        if (Hash::check($request->input('password'), $Credencial->password)) {
            return [
                'token' => $this->jwt($Credencial),
                'UserId' => $Credencial->id,
                'Email' => $Credencial->email,
                'expires_in' => $this->secondsToTime(60 * 60 * 24)
            ];
        }
        // Bad Request response
        return response()->json([
            'error' => 'Email or password is wrong.'
        ], 401);
    }

    /**
     * Decodifica token jwt
     */
    public static function decode($jwt)
    {
        return JWT::decode($jwt, env('JWT_SECRET'), ['HS256']);
    }

    public static function checarToken($request)
    {
        $authorization = $request->headers->get("Authorization");
        list($jwt) = sscanf($authorization, 'Bearer %s');

        if ($jwt) {
            try {
                $status = 200;
                // Use self:: instead of $this-> for static methods.
                $app['jwt'] = self::decode($jwt); //JWTWrapper::decode($jwt);
            } catch (Exception $error) {
                CustomLog::log($error->getMessage(), 'ERROR', 'api/api-', 'api');
                $status = $error->getCode() === 0 ? 500 : $error->getCode();
                $app['error'] = true;

                return new Response('Acesso nao autorizado', 401);
            } finally {
                return response()->json($status);
            }
        } else {
            return new Response('Token nao informado', 400);
        }
    }

    /**
     * Convert number of seconds into hours, minutes and seconds
     * and return an array containing those values
     *
     * @param integer $inputSeconds Number of seconds to parse
     * @return array
     */

    public function secondsToTime($inputSeconds)
    {
        $secondsInAMinute = 60;
        $secondsInAnHour = 60 * $secondsInAMinute;
        $secondsInADay = 24 * $secondsInAnHour;

        // Extract days
        $days = floor($inputSeconds / $secondsInADay);

        // Extract hours
        $hourSeconds = $inputSeconds % $secondsInADay;
        $hours = floor($hourSeconds / $secondsInAnHour);

        // Extract minutes
        $minuteSeconds = $hourSeconds % $secondsInAnHour;
        $minutes = floor($minuteSeconds / $secondsInAMinute);

        // Extract the remaining seconds
        $remainingSeconds = $minuteSeconds % $secondsInAMinute;
        $seconds = ceil($remainingSeconds);

        // Format and return
        $timeParts = [];
        $sections = [
            'day' => (int)$days,
            'hour' => (int)$hours,
            'minute' => (int)$minutes,
            'second' => (int)$seconds,
        ];

        foreach ($sections as $name => $value) {
            if ($value > 0) {
                $timeParts[] = $value . ' ' . $name . ($value == 1 ? '' : 's');
            }
        }

        return implode(', ', $timeParts);
    }
}
