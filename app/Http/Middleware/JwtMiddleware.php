<?php

namespace App\Http\Middleware;

use App\Helpers\CustomLog;
use Closure;
use Exception;
use App\Models\Api\Credencial;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

/**
 * @category Middleware
 * @package  App\Http\Middleware\
 * @author   Renato Lucena <cpdrenato@gmail.com>
 * @copyright (c) 2021-2022 Renato Lucena
 * @license Copyright 2022 Klios. Released under dual MIT and GPL licenses.
 * @link     https://renatolucena.net
 */

class JwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */

    public function handle($request, Closure $next, $guard = null)
    {
        // $token = $request->header('token');
        $token = $request->headers->get("Authorization");
        list($jwt) = sscanf($token, 'Bearer %s');
        if (!$token) {
            CustomLog::log('CREDENCIAL API TOKEN REQUERIDO', 'INFO', 'api/api-', 'api');
            return response()->json([
                'error' => 'Token requerido'
            ], 401);
        }
        try {
            $credentials = JWT::decode($jwt, env('JWT_SECRET'), ['HS256']);
        } catch (ExpiredException $e) {
            CustomLog::log('CREDENCIAL API TOKEN EXPIRADO', 'INFO', 'api/api-', 'api');
            return response()->json([
                'error' => 'Token Expirado'
            ], 400);
        } catch (Exception $e) {
            return response()->json([
                'error' => 'Ocorreu um erro na decodificação'
            ], 400);
        }

        $user = Credencial::find($credentials->sub);
        $request->auth = $user;
        return $next($request);
    }
}
