<?php

namespace App\Http\Resources;

//eloquent-resources
class ConsultaEmailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'error' => false,
            'errors' => array(),
            'model' => [
                'consultaEmail' => [
                    'id' => $this->id,
                    'consulta_id' => $this->consulta_id,
                    'usuario_id' => $this->usuario_id,
                    'posted_at' => $this->posted_at,
                    'post' => $this->post,
                    'usuario' => $this->whenLoaded('usuario'),
                    'consulta' => $this->whenLoaded('consulta'),
                    'created_at' => $this->created_at,
                    'updated_at' => $this->updated_at,
                    'deleted_at' => $this->deleted_at
                ]
            ],
            'collection' => [],
            'data' => [],
        ];
    }
}
