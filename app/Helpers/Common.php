<?php

// namespace App\Helpers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Determina se a variável é vazia.
 * Uma variável é considerada vazia se não existir ou seu valor é igual FALSE.
 * A função empty() não gera um aviso se a variável não existir.
 * Retorna true quando:
 *
        $var = ""; (uma string vazia)
        $var = 0; (um inteiro valendo zero)
        $var = "0"; (uma string contendo zero)
        $var = NULL; (variáveis nulas)
        $var = FALSE; (variáveis falsas)
        $var = array(); (um array vazio)
        var $var; (uma variável declarada, sem valor, dentro de uma classe)
 */
if (! function_exists('validarEmBranco')) {
    function validarEmBranco($valor)
    {
        return empty($valor);
    }
}

/**
 * Verifica se uma variavel existe.
 * Retorna TRUE quando a variavel existir
 */
if (! function_exists('variavelExiste')) {
    function variavelExiste($valor)
    {
        return isset($valor);
    }
}

/**
 *  Printa na Tela o valor passado formatado
 *  Com objetivo de melhorar o debugger
 */
if (! function_exists('printPre')) {
    function printPre($valor)
    {
        print("<pre>");
        print_r($valor);
        print("</pre>");
    }
}

/**
 * Printa na tela o valor passado e formatada.
 * Retorna o tipo do valor
 */
if (! function_exists('varDump')) {
    function varDump($valor)
    {
        print("<pre>");
        var_dump($valor);
        print("</pre>");
    }
}

/**
 * Verifica se foi informado um id para Nao Conformidade.
 * Se foi informado, valida se ele for informado criptografado
 * ou não. Retorna valor, com base no valor informado.
 */
if (! function_exists('tratarIdNC')) {
    function tratarIdNC($valor)
    {
        if (empty($valor)) {
            return null;
        }

        if (is_numeric($valor)) {
            return (int) $valor;
        }

        return \Illuminate\Support\Facades\Crypt::decrypt($valor);
    }
}

/**
 * Verifica se o usuario que cadastrou NC é o mesmo que
 * esta tentando editar a NC. Senão for o mesmo usuario,
 * retorna false;
 */
if (! function_exists('isAuthorNC')) {
    function isAuthorNC(int $usuarioId, int $autorId): bool
    {
        if ($usuarioId === $autorId) {
            return true;
        }
        return false;
    }
}

/**
 * Verifica se o papel do Usuário tem permissão de alterar
 * a NC cadastrada. Apenas papeias abaixo podem editar:
 * 3 - Analistas
 * 4 - Gestores
 * 5 - Diretores
 * 6 - Herois (Desenvolvedores)
 */
if (! function_exists('usuarioPodeAlterarNC')) {
    function usuarioPodeAlterarNC(int $papelId): bool
    {
        if (in_array($papelId, [3,4,6])) {
            return true;
        }
        return false;
    }
}

/**
 * @name verificaAutorizacaoParaEditarNC
 * @description Verifica se o usuario logado tem permissão para editar a NC
 * @return Return False se houver autorizacao
 */
if (! function_exists('verificaAutorizacaoParaEditarNC')) {
    function verificaAutorizacaoParaEditarNC(int $usuarioId, int $autorId, int $papelUsuario)
    {
        if (isAuthorNC($usuarioId, $autorId)) {
            return true;
        }

        if (usuarioPodeAlterarNC($papelUsuario)) {
            return true;
        }
        return false;
    }
}


if (! function_exists('retiraAcentos')) {
    /**
     * Retira acentos da string
     *
     * @param string $str String
     *
     * @return string String
     */
    function retiraAcentos($str)
    {
        /*$string = 'ÁÍÓÚÉÄÏÖÜËÀÌÒÙÈÃÕÂÎÔÛÊáíóúéäïöüëàìòùèãõâîôûêÇç?';*/
        $charset = 'utf-8';
        $str = htmlentities($str, ENT_NOQUOTES, $charset);
        $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str);
        $str = preg_replace('#&[^;]+;#', '', $str);
        return $str;
    }
}


if (! function_exists('gerarDataInicioMes')) {
    function gerarDataInicioMes()
    {
        return date('Y-m-01');
    }
}

if (! function_exists('gerarDataFimMes')) {
    function gerarDataFimMes()
    {
        return date('Y-m-t');
    }
}


if (!function_exists('formataDataPadraoPostgres')) {
    function formataDataPadraoPostgres($data)
    {
        if ($data) {
            $data = str_replace('/', '-', $data);
            $arrayDatas = explode('-', $data);

            $data =  \Carbon\Carbon::createMidnightDate($arrayDatas[2], $arrayDatas[1], $arrayDatas[0]);

            return  $data;
        }

        return;
    }
}

if (!function_exists('formataData')) {
    function formataData($data)
    {
        if ($data) {
            $data = str_replace('/', '-', $data);

            return  $data;
        }
        return;
    }
}

if (!function_exists('formataDataInicial')) {
    function formataDataInicial($data)
    {
        if ($data) {
            $data = str_replace('/', '-', $data);
            $arrayDatas = explode('-', $data);

            $data = \Carbon\Carbon::createMidnightDate($arrayDatas[0], $arrayDatas[1], $arrayDatas[2]);
            ;

            return $data;
        }
        return;
    }
}

if (!function_exists('formataDataFinal')) {
    function formataDataFinal($data)
    {
        if ($data) {
            $data = str_replace('/', '-', $data);
            $arrayDatas = explode('-', $data);

            $data = \Carbon\Carbon::createMidnightDate($arrayDatas[0], $arrayDatas[1], $arrayDatas[2]);
            ;

            return $data->addDay(1)->subSecond(1);
        }
        return;
    }
}



if (!function_exists('getRota')) {
    function getRota()
    {
        $url = \Illuminate\Support\Facades\URL::current();
        $vUrl = explode('/', $url);
        $ultimo = end($vUrl);

        $valor = prev($vUrl);

        return  $valor;
    }
}


if (!function_exists('mascara')) {
    /**
     * Aplica a máscara a string
     *
     * @param String $valor   Valor a ser formatado
     * @param String $mascara Valor da máscara. Ex: #####-###
     *
     * @return type
     **/
    function mascara($valor, $mascara)
    {
        $valorComMascara = '';

        if ($valor != '') {
            $k = 0;

            for ($i = 0; $i <= strlen($mascara) - 1; $i++) {
                if ($mascara[$i] == '#') {
                    if (isset($valor[$k])) {
                        $valorComMascara .= $valor[$k++];
                    }
                } else {
                    if (isset($mascara[$i])) {
                        $valorComMascara .= $mascara[$i];
                    }
                }
            }
        }

        return $valorComMascara;
    }
}

if (!function_exists('listarEnum')) {
    /**
     * Lista ítens do enum
     *
     * @param String  $enum Nome do tipo enum
     * @param boolean $sort Se o valor deve ou não ser sorteado
     *
     * @return type
     **/
    function listarEnum($enum, $sort = true)
    {
        $results = DB::select(DB::raw("SELECT unnest(enum_range(NULL::{$enum})) AS item"));
        if ($sort) {
            sort($results);
        }
        return $results;
    }
}

if (!function_exists('isSoftDelete')) {
    /**
     * Verifica se o objeto utiliza ou não o SoftDelete
     *
     * @param Model $model Istância do objeto a ser verificado
     *
     * @return bool
     **/
    function isSoftDelete($model)
    {
        return in_array(
            'Illuminate\Database\Eloquent\SoftDeletes',
            class_uses($model)
        ) && !$model->forceDeleting;
    }
}


if (!function_exists('keepOnly')) {
    /**
     * Preserva no array apenas os valores das chaves passadas
     *
     * @param string $keys  Chaves do array
     * @param int    $array Array a ser limpado
     *
     * @return bool
     **/
    function keepOnly($keys, $array)
    {
        return array_intersect_key($array, array_flip($keys));
    }
}

if (!function_exists('alterarValorSequencia')) {
    /**
     * Reseta a sequência do postgres
     *
     * @param string $sequencia Nome da sequência
     * @param string $valor     Valor a ser alterado
     *
     * @return void
     **/
    function alterarValorSequencia($sequencia, $valor = 1)
    {
        DB::statement(
            "SELECT setval('{$sequencia}', {$valor}, false)"
        );
    }
}

if (!function_exists('keepOnly')) {
    /**
     * Preserva no array apenas os valores das chaves passadas
     *
     * @param string $keys  Chaves do array
     * @param int    $array Array a ser limpado
     *
     * @return bool
     **/
    function keepOnly($keys, $array)
    {
        return array_intersect_key($array, array_flip($keys));
    }
}


if (!function_exists('comboBox')) {
    /**
     * Gera uma combobox de acordo com uma collection
     *
     * @param string  $collection     Coleção
     * @param string  $selectedId     Ítem selecionado
     * @param boolean $opcaoEmBranco  Boolean
     * @param boolean $labelCaixaAlta Boolean
     *
     * @return void
     **/
    function comboBox($collection, $selectedId = null, $opcaoEmBranco = true, $labelCaixaAlta = false)
    {
        $comboBox = '';
        if ($opcaoEmBranco == true) {
            if ($labelCaixaAlta) {
                $comboBox = "<option value=''>SELECIONAR...</option>";
            } else {
                $comboBox = "<option value=''>Selecionar...</option>";
            }
        }

        if ($collection) {
            foreach ($collection as $item) {
                $selected = ($selectedId == $item['value'] ? 'selected': '');
                if ($labelCaixaAlta) {
                    $item['label'] = mb_strtoupper($item['label'], 'UTF-8');
                }
                $comboBox.="<option value='{$item['value']}' {$selected}>{$item['label']}</option>";
            }
        }
        return $comboBox;
    }
}

if (!function_exists('comboMultiple')) {
    /**
     * Gera uma combobox de acordo com uma collection
     *
     * @param string $collection     Coleção
     * @param string $selectedIds    Ítem selecionado
     * @param string $labelCaixaAlta Ítem selecionado
     *
     * @return void
     **/
    function comboMultiple($collection, $selectedIds = [], $labelCaixaAlta = false)
    {
        $comboBox = '';
        if ($collection->count() > 0) {
            foreach ($collection as $item) {
                $selected = (in_array($item['value'], $selectedIds)? "selected='selected'": '');
                if ($labelCaixaAlta) {
                    $item['label'] = mb_strtoupper($item['label'], 'UTF-8');
                }
                $comboBox.="<option value='{$item['value']}' {$selected}>{$item['label']}</option>";
            }
        }
        return $comboBox;
    }
}

if (!function_exists('comboCheckboxes')) {
    function comboCheckboxes($collection, $checkeds = [])
    {
        if ($collection) {
            $collection = $collection->map(function ($item) use ($checkeds) {
                $item->checked = null;

                if (in_array($item->value, $checkeds)) {
                    $item->checked = 'checked';
                }

                return $item;
            });
        }

        return $collection;
    }
}


if (!function_exists('toDelimitedString')) {

    /**
     * Convert an array of strings to a delimited string. This function supports CSV as well as SQL output since
     * the quote character is customisable and the escaping behaviour is the same for CSV and SQL.
     *
     * Tests:
     *  echo toDelimitedString([], ',', '\'', true) . "\n";
     *  echo toDelimitedString(['A'], ',', '\'', true) . "\n";
     *  echo toDelimitedString(['A', 'B'], ',', '\'', true) . "\n";
     *  echo toDelimitedString(['A', 'B\'C'], ',', '\'', true) . "\n";
     *  echo toDelimitedString([], ',', '\'', true) . "\n";
     *  echo toDelimitedString(['A'], ',', '"', true) . "\n";
     *  echo toDelimitedString(['A', 'B'], ',', '"', true) . "\n";
     *  echo toDelimitedString(['A', 'B"C'], ',', '"', true) . "\n";
     *
     * Outputs:
     *  <Empty String>
     *  'A'
     *  'A','B'
     *  'A','B''C'
     *  <Empty String>
     *  "A"
     *  "A","B"
     *  "A","B""C"
     *
     * @param array  $array     A one-dimensional array of string literals
     * @param string $delimiter The character to separate string parts
     * @param string $quoteChar The optional quote character to surround strings with
     * @param bool   $escape    Flag to indicate whether instances of the quote character should be escaped
     *
     * @return string
     */
    function toDelimitedString(
        array $array,
        string $delimiter = ';',
        string $quoteChar = '"',
        bool $escape = true
    ) {
        // Escape the quote character, since it is somewhat expensive it can be suppressed
        if ($escape && !empty($quoteChar)) {
            $array = str_replace($quoteChar, $quoteChar . $quoteChar, $array);
        }

        // Put quotes and commas between all the values
        $values = implode($array, $quoteChar . $delimiter . $quoteChar);

        // Put first and last quote around the list, but only if it is not empty
        if (strlen($values) > 0) {
            $values = $quoteChar . $values . $quoteChar;
        }

        return $values;
    }
}

if (!function_exists('arrayToCsv')) {

    /**
     * Converte um array em uma string CSV
     *
     * @param array $array Array
     *
     * @return void
     */
    function arrayToCsv(array $linhas)
    {
        $relatorioCsv = '';

        foreach ($linhas as $linha) {
            $relatorioCsv .= toDelimitedString($linha).PHP_EOL;
        }

        return $relatorioCsv;
    }
}

if (!function_exists('converterDataBrToEn')) {

    /**
     * Converte a data em String de Pt-br
     *
     * @param array $date String
     *
     * @return void
     */
    function converterDataBrToEn($date = '')
    {
        if ($date != '') {
            $date = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
        }
        return $date;
    }
}

if (!function_exists('converterDataEnToBr')) {

    /**
     * Converte a data em String de En to Pt-br
     *
     * @param array $date String
     *
     * @return void
     */
    function converterDataEnToBr($date = '')
    {
        if ($date != '') {
            $date = Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
        }
        return $date;
    }
}

if (!function_exists('ExtrairTelefone')) {

    /**
     * @param String $numero
     */
    function ExtrairTelefone($numero = null)
    {
        return preg_replace("/[^0-9]/", "", $numero);
    }
}

if (!function_exists('TelefoneValido')) {
    /**
     * Valida telefone
     *
     * @param String $numero Número a ser verificado
     *
     * @return boolean
     */
    function TelefoneValido($numero = null)
    {
        $numero = ExtrairTelefone($numero);
        return strlen($numero) >= 8;
    }
}


if (!function_exists('nextval')) {
    /**
     * Retorna o proximo valor da sequência
     *
     * @param string $sequencia Nome da sequência
     *
     * @return void
     **/
    function nextVal($sequencia)
    {
        $valor = DB::select("SELECT nextval('{$sequencia}')");
        return $valor[0]->nextval;
    }
}

if (!function_exists('transformarTempoEmSegundos')) {
    /**
     * Transforma tempo em HH:MM:SS em segundos
     *
     * @param string $valor valor
     * @return int
     */
    function transformarTempoEmSegundos($valor)
    {
        sscanf($valor, "%d:%d:%d", $horas, $minutos, $segundos);

        $totalSegundos = ($horas * 3600) + ($minutos * 60) + $segundos;

        return $totalSegundos;
    }
}

if (!function_exists('transformarSegundosEmTempo')) {
    /**
     * Transforma segundos em HH:MM:SS
     *
     * @param int $valor valor
     * @return void
     */
    function transformarSegundosEmTempo($valor)
    {
        $horas = floor($valor / 3600);
        $minutos = floor(($valor - ($horas * 3600)) / 60);
        $segundos = floor($valor % 60);

        $minutos = str_pad($minutos, 2, "0", STR_PAD_LEFT);
        $segundos = str_pad($segundos, 2, "0", STR_PAD_LEFT);

        $tempo = $horas . ':' . $minutos . ':' . $segundos;

        return $tempo;
    }
}

if (!function_exists('DECtoDMS')) {
    function DECtoDMS($dec)
    {
        $formato = "%s°%s'%s\"%s";
        $latlong = explode(',', $dec);

        $latitudeCardeal = 'N';
        if (trim($latlong[0]) <= 0) {
            $latitudeCardeal = 'S';
        }
        $latitude = explode(".", $latlong[0]);

        $degLat = $latitude[0];
        if ($degLat <= 0) {
            $degLat*=-1;
        }
        $tempmaLat = "0.".$latitude[1];
        $tempmaLat = $tempmaLat * 3600;
        $minLat = floor($tempmaLat / 60);
        $secLat = $tempmaLat - ($minLat * 60);

        $minLat = str_pad($minLat, 2, '0', STR_PAD_LEFT);
        $secLat = str_pad(sprintf("%0.1f", $secLat), 4, '0', STR_PAD_LEFT);
        $latitudeFormatada = trim(sprintf($formato, $degLat, $minLat, $secLat, $latitudeCardeal));

        $longitudeCardeal = 'E';
        if (trim($latlong[1]) <= 0) {
            $longitudeCardeal = 'W';
        }
        $longitude = explode(".", $latlong[1]);

        $degLong = $longitude[0];

        if ($degLong <= 0) {
            $degLong*=-1;
        }

        $tempmaLong = "0.".$longitude[1];
        $tempmaLong = $tempmaLong * 3600;
        $minLong = floor($tempmaLong / 60);
        $secLong = $tempmaLong - ($minLong * 60);

        $minLong = str_pad($minLong, 2, '0', STR_PAD_LEFT);
        $secLong = str_pad(sprintf("%0.1f", $secLong), 4, '0', STR_PAD_LEFT);
        $longitudeFormatada = trim(sprintf($formato, $degLong, $minLong, $secLong, $longitudeCardeal));

        return $latitudeFormatada.' '.$longitudeFormatada;
    }
}

if (!function_exists('validarData')) {
    /**
     * Valida uma data
     *
     * @param string $valor
     * @param string $formatoData
     * @return void
     */
    function validarData($valor, $formatoData)
    {
        return DateTime::createFromFormat($formatoData, $valor) !== false;
    }
}

if (!function_exists('validarIntegerRange')) {
    function validarIntegerRange($valor, $range)
    {
        $valido = false;

        if (strlen($valor) <= $range) {
            $valido = true;
        }

        return $valido;
    }
}

if (!function_exists('listarCabecalhoEmail')) {
    function listarCabecalhoEmail($lista)
    {
        $listaEmails = $lista['emails'];

        //Primeiro destinatário da lista
        $lista['destinatario'] = $listaEmails->first();
        $lista['destinatario'] = $lista['destinatario']['email'];

        //Todos os emails da lista, exceto o primeiro
        $lista['cc'] = $listaEmails->slice(1);
        $lista['cc'] = $lista['cc']->pluck('email')->toArray();

        //Todos os emails da lista
        $lista['destinatarios'] = $listaEmails->pluck('email')->toArray();

        return $lista;
    }
}

if (!function_exists('csvToArray')) {
    function csvToArray($file, $delimitador, $headersKeys = true)
    {
        ini_set('memory_limit', '8192M');

        $dados = [];

        if (($handle = fopen($file, "r")) !== false) {
            while (($data = fgetcsv($handle, 1000, $delimitador)) !== false) {
                $num = count($data);
                $array = [];

                for ($c = 0; $c < $num; $c++) {
                    $array[] = trim($data[$c]);
                }

                $dados[] =  $array;
            }

            fclose($handle);
        }

        foreach ($dados[0] as $key => $dado) {
            $dado = mb_strtolower(retiraAcentos($dado));
            $dado = preg_replace('/[^0-9a-zA-Z]/', '', $dado);

            $dados[0][$key] = $dado;
        }

        if ($headersKeys) {
            $cabecalho = array_shift($dados);
            $dadosBlocos = array_chunk($dados, 10000, true);

            foreach ($dadosBlocos as $bloco) {
                foreach ($bloco as $keyDado => $dado) {
                    $dado = array_combine($cabecalho, $dado);

                    $dados[$keyDado] = $dado;
                }
            }

            array_unshift($dados, $cabecalho);
        }

        return $dados;
    }
}

if (!function_exists('gerarImagemPreview')) {
    function gerarImagemPreview($arquivo)
    {
        $urlBase = config('app.url');

        $arquivo->url = $urlBase . '/storage/' . $arquivo->sha256;
        $arquivo->src = $arquivo->url;

        if ($arquivo->extensao == 'pdf') {
            $arquivo->src = '/images/pdf2.png';
        } elseif ($arquivo->extensao == 'mp4' || $arquivo->extensao == 'avi') {
            $arquivo->src = '/images/video_thumbnail.jpg';
        }

        return $arquivo;
    }
}

if (!function_exists('removerMascaraDinheiro')) {
    function removerMascaraDinheiro($valor)
    {
        if ($valor) {
            $valor = preg_replace('/([^0-9\,])/i', '', $valor);
            $valor = preg_replace('/,/i', '.', $valor);
        } else {
            $valor = null;
        }

        return $valor;
    }
}

if (!function_exists('converterParametrosBrancosEmNull')) {
    function converterParametrosBrancosEmNull($dados) {
        foreach ($dados as $key => $dado) {
            if ($dado === '') {
                $dados[$key] = null;
            }
        }

        return $dados;
    }
}
