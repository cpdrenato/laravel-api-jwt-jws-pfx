<?php

namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CustomLog extends Model {

    public static function log($mensagem, $tipo = "ERROR", $nomeArquivo = null, $pasta = null) 
    {
        $nomeArquivo = $nomeArquivo ?? "customlog";
        $nomeArquivo.= Carbon::now()->format('Y-m-d').'.log';
        
        $cabecalho = "[".Carbon::now()->format('Y-m-d H:i:s')."] lumen.{$tipo}: ";

        if ($pasta) {
            $pasta = storage_path() . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . $pasta;

            if (!file_exists($pasta)) {
                mkdir($pasta);
            }
        }

        $binario = fopen(storage_path() . DIRECTORY_SEPARATOR.'logs' . DIRECTORY_SEPARATOR . $nomeArquivo, 'a');
        
        fwrite($binario, $cabecalho.$mensagem.PHP_EOL);
        fclose($binario);

    }
}
