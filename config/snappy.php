<?php

if (env('APP_ENV', 'production') == 'production') {
    return array(
        'pdf' => array(
            'enabled' => true,
            'binary' => '/bin/wkhtmltopdf', //FOR LINUX
            'timeout' => false,
            'options' => array(/*'margin-top' => '25mm'*/),
            ///*'options' => array(
            // 'zoom' => 1.2,
            // 'margin-top' => '30mm' //FOR WINDOWS
            // 'margin-top' => '25mm', //FOR LINUX
            //),*/
            'env' => array(),
        ),
        'image' => array(
            'enabled' => true,
            'binary' => base_path('vendor/h4cc/wkhtmltoimage-amd64/bin/wkhtmltoimage-amd64'), //FOR LINUX
            'timeout' => false,
            'options' => array(
                //'zoom' => 1.2 //FOR WINDOWS
            ),
            'env' => array(),
        ),
    );
} else if (env('APP_ENV', 'homolog') == 'homolog') {
    return array(
        'pdf' => array(
            'enabled' => true,
            // 'binary' => base_path('vendor\wemersonjanuario\wkhtmltopdf-windows\bin\64bit\wkhtmltopdf'), //FOR WINDOWS
            'binary' => '/bin/wkhtmltopdf', //FOR LINUX
            'timeout' => false,
            'options' => array(/*'margin-top' => '25mm'*/),
            ///*'options' => array(
            // 'zoom' => 1.2,
            // 'margin-top' => '30mm' //FOR WINDOWS
            // 'margin-top' => '25mm', //FOR LINUX
            //),*/
            'env' => array(),
        ),
        'image' => array(
            'enabled' => true,
            // 'binary'   => base_path('vendor\wemersonjanuario\wkhtmltopdf-windows\bin\64bit\wkhtmltoimage'), //FOR WINDOWS
            'binary' => base_path('vendor/h4cc/wkhtmltoimage-amd64/bin/wkhtmltoimage-amd64'), //FOR LINUX
            'timeout' => false,
            'options' => array(
                //'zoom' => 1.2 //FOR WINDOWS
            ),
            'env' => array(),
        ),
    );
} else {
    return array(
        'pdf' => array(
            'enabled' => true,
            'binary' => base_path('vendor\wemersonjanuario\wkhtmltopdf-windows\bin\64bit\wkhtmltopdf'), //FOR WINDOWS
            //'binary' => '/bin/wkhtmltopdf', //FOR LINUX
            // 'binary' => '/var/www/wkhtmltopdf-amd64', // pc Renato
            'timeout' => false, // 300
            'options' => array(/*'margin-top' => '25mm'*/),
            'env' => array(),
        ),

        'image' => array(
            'enabled' => true,
            'binary'   => base_path('vendor\wemersonjanuario\wkhtmltopdf-windows\bin\64bit\wkhtmltoimage'), //FOR WINDOWS
            //'binary' => base_path('vendor/h4cc/wkhtmltoimage-amd64/bin/wkhtmltoimage-amd64'), //FOR LINUX
            'timeout' => false,
            'options' => array(
                //'zoom' => 1.2 //FOR WINDOWS
            ),
            'env' => array(),
        ),
    );
}

    /*
    |--------------------------------------------------------------------------
    | Snappy PDF / Image Configuration
    |--------------------------------------------------------------------------
    |
    | This option contains settings for PDF generation.
    |
    | Enabled:
    |
    |    Whether to load PDF / Image generation.
    |
    | Binary:
    |
    |    The file path of the wkhtmltopdf / wkhtmltoimage executable.
    |
    | Timout:
    |
    |    The amount of time to wait (in seconds) before PDF / Image generation is stopped.
    |    Setting this to false disables the timeout (unlimited processing time).
    |
    | Options:
    |
    |    The wkhtmltopdf command options. These are passed directly to wkhtmltopdf.
    |    See https://wkhtmltopdf.org/usage/wkhtmltopdf.txt for all options.
    |
    | Env:
    |
    |    The environment variables to set while running the wkhtmltopdf process.
    |
    */


