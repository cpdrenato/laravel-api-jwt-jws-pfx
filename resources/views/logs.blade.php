<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Logs</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>

        </style>

        <style>
            body {
                font-family: 'Nunito';
            }
        </style>
    </head>
    <body class="">
        <div class="container">
            <br>
            System Build v{{ Illuminate\Foundation\Application::VERSION }}
            <br>
        {{-- A way to change the log file date --}}
        <form action="{{ route('logs') }}">
            <input type="date" name="date" value="{{ $date ? $date->format('Y-m-d') : today()->format('Y-m-d') }}">
            <button type="submit">Get</button>
        </form>

        {{-- View log file info and contents --}}
        @if (empty($data['file']))
            <div>
                <h3>No Logs Found</h3>
            </div>
        @else
            <div>
                <h5>Update On : <b>{{ $data['lastModified']->format('Y-m-d:i a') }}</b></h5>
                <h5>File Size : <b>{{ round($data['size'] / 1024) }} KB</b></h5>
                <pre>{{ $data['file'] }}</pre>
            </div>
        @endif
        </div>
    </body>
</html>
