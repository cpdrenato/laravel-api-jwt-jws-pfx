<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCredencialTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        schema::create('credencial_token', function (Blueprint $table) {
            $table->increments('id');
            $table->string('token');
            $table->integer('credencial_id');
            $table->timestamps();
            $table->foreign('credencial_id')
                ->references('id')->on('adagio_api_credenciais')
                ->onDelete('cascade');
        });
        \Illuminate\Support\Facades\DB::statement("SELECT setval(pg_get_serial_sequence('credencial_token', 'id'), coalesce((max(id)+1),1), false) FROM credencial_token;");
        //SELECT setval(pg_get_serial_sequence('users', 'id'), coalesce(max(id)+1, 1), false) FROM users;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credencial_token');
    }
}
