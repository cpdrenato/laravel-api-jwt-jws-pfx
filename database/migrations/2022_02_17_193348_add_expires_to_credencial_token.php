<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpiresToCredencialToken extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('credencial_token', function (Blueprint $table) {
            // $table->dateTime('expires_at')->nullable();
            $table->dateTime('expires_at')->after('created_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('credencial_token', function (Blueprint $table) {
            $table->dropColumn(['expires_at']);
        });
    }
}
