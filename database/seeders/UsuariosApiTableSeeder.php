<?php

namespace Database\Seeders;

use App\Models\User as Credencial;
use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UsuariosApiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Credencial::where('id', 1)->count() == 0) {
            Credencial::create(
                [
                    'name' => 'teste',
                    'email' => 'teste@teste',
                    'password' => Hash::make('Galileu_ADAGIO@API$'),
                ]
            );
        }

        if (Credencial::where('id', 2)->count() == 0) {

            Credencial::create(
                [
                    'name' => 'test',
                    'email' => 'test@test.com.br',
                    'password' => Hash::make('Dpa@Fst_2020$Mult1ct3'),
                ]
            );
        }

        if (Credencial::where('id', 3)->count() == 0) {

            Credencial::create(
                [
                    'name' => 'user',
                    'email' => 'user@user.com',
                    'password' => Hash::make('user$FST_2021'),
                ]
            );
        }

        if (Credencial::where('id', 4)->count() == 0) {

            Credencial::create(
                [
                    'name' => 'apiteste',
                    'email' => 'apiteste@teste.com',
                    'password' => Hash::make('teste_2021'),
                ]
            );
        }

        $this->command->info("Total de Credenciais: " . Credencial::get()->count());
        $this->command->info("Suas informacoes foram atualizadas!");
    }
}
